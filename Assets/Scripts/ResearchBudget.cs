﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchBudget : MonoBehaviour {
    public GameManager gamemanager;
    public PlaneDesigner designer;

    public Sprite sliderLock;
    public Sprite sliderUnlock;
    public List<PlaneModule> currentModules = new List<PlaneModule>();
    public List<PlaneModule> WingModules = new List<PlaneModule>();
    public List<PlaneModule> LandingModules = new List<PlaneModule>();
    public List<PlaneModule> TailModules = new List<PlaneModule>();
    public List<PlaneModule> EngineModules = new List<PlaneModule>();
    public List<PlaneModule> CockpitModules = new List<PlaneModule>();
    public List<PlaneModule> CabinModules = new List<PlaneModule>();
    public List<PlaneModule> FuselageeModules = new List<PlaneModule>();
    private List<PlaneModule> UnlockableModules;

    public GameObject popUpWindow;
    public InputField budgetInput;
    public Button splitEven;
    [Header("Images")]
    public Image engineImage;
    public Image cockpitImage;
    public Image wingsImage;
    public Image landingImage;
    public Image tailImage;
    public Image cabinImage;
    public Image fuselageImage;
    [Header("Sliders")]
    public Slider engineSlider;
    public Slider cockpitSlider;
    public Slider wingsSlider;
    public Slider landingSlider;
    public Slider tailSlider;
    public Slider cabinSlider;
    public Slider fuselageSlider;
    [Header("Texts")]
    public Text engineText;
    public Text cockpitText;
    public Text wingsText;
    public Text landingText;
    public Text tailText;
    public Text cabinText;
    public Text fuselageText;

    public Button engineButton;
    public Button cockpitButton;
    public Button wingsButton;
    public Button landingButton;
    public Button tailButton;
    public Button cabinButton;
    public Button fuselageButton;

    [Header("Small buttons")]
    public List<Button> SmallPartButtons;
    [Header("Medium buttons")]
    public List<Button> MediumPartButtons;
    [Header("Huge buttons")]
    public List<Button> HugePartButtons;

    private bool engineLock = false;
    private bool cockpitLock = false;
    private bool wingsLock = false;
    private bool landingLock = false;
    private bool tailLock = false;
    private bool cabinLock = false;
    private bool fuselageLock = false;
    [Header("Values")]
    public float researchBudget;
    public float remainingBudget;



    public float engineCredits = 0;
    public float cockpitCredits = 0;
    public float wingsCredits = 0;
    public float landingCredits = 0;
    public float tailCredits = 0;
    public float cabinCredits = 0;
    public float fuselageCredits = 0;

    public float engineBudget = 0;
    public float cockpitBudget = 0;
    public float wingsBudget = 0;
    public float landingBudget = 0;
    public float tailBudget  = 0;
    public float cabinBudget = 0;
    public float fuselageBudget = 0;

    // Use this for initialization
    public void Start () {
        UnlockableModules = ModuleCreator.UnlockableModules();
        designer = gamemanager.GetDesignerScript();
        
        researchBudget = 1000;
        engineSlider.onValueChanged.AddListener(delegate { UpdateBudgets(engineSlider); });
        
        cockpitSlider.onValueChanged.AddListener(delegate { UpdateBudgets(cockpitSlider); });
        wingsSlider.onValueChanged.AddListener(delegate { UpdateBudgets(wingsSlider); });
        landingSlider.onValueChanged.AddListener(delegate { UpdateBudgets(landingSlider); });
        tailSlider.onValueChanged.AddListener(delegate { UpdateBudgets(tailSlider); });
        cabinSlider.onValueChanged.AddListener(delegate { UpdateBudgets(cabinSlider); });
        fuselageSlider.onValueChanged.AddListener(delegate { UpdateBudgets(fuselageSlider); });
        UpdateTexts();
        popUpWindow.SetActive(false);

        remainingBudget = researchBudget;

        engineSlider.maxValue = remainingBudget;
        cockpitSlider.maxValue = remainingBudget;
        wingsSlider.maxValue = remainingBudget;
        landingSlider.maxValue = remainingBudget;
        tailSlider.maxValue = remainingBudget;
        cabinSlider.maxValue = remainingBudget;
        fuselageSlider.maxValue = remainingBudget;


        engineButton.interactable = false;
        cockpitButton.interactable = false;
        wingsButton.interactable = false;
        landingButton.interactable = false;
        tailButton.interactable = false;
        cabinButton.interactable = false;
        fuselageButton.interactable = false;

        cockpitButton.GetComponent<Button>().onClick.AddListener(() => GetUnlockableParts(PartType.Cockpit));
        engineButton.GetComponent<Button>().onClick.AddListener(() => GetUnlockableParts(PartType.Engine));
        wingsButton.GetComponent<Button>().onClick.AddListener(() => GetUnlockableParts(PartType.Wing));
        landingButton.GetComponent<Button>().onClick.AddListener(() => GetUnlockableParts(PartType.LandingGear));
        tailButton.GetComponent<Button>().onClick.AddListener(() => GetUnlockableParts(PartType.Tail));


    }

    public void GetUnlockableParts(PartType type)
    {
        int i = 0;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == type&&module.supportedFuselage ==Fuselage.Tiny)
            {
                SmallPartButtons[i].gameObject.SetActive(true);
                SmallPartButtons[i].GetComponentInChildren<Text>().text = module.name+" "+module.basePrice.ToString()+"$";
                SmallPartButtons[i].onClick.RemoveAllListeners();
                PlaneModule tempModule = module;
                int tempI = i;
                SmallPartButtons[i].onClick.AddListener(() => designer.AddModuleToUnlocked(tempModule));
                SmallPartButtons[i].onClick.AddListener(() => SmallPartButtons[tempI].gameObject.SetActive(false));
                SmallPartButtons[i].onClick.AddListener(() => UnlockableModules.Remove(tempModule));

                i++;
            }
        }
        while (i < SmallPartButtons.Count)
        {
            SmallPartButtons[i].gameObject.SetActive(false);
            i++;
        }
        int k = 0;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == type && module.supportedFuselage == Fuselage.Medium)
            {
                MediumPartButtons[k].gameObject.SetActive(true);
                MediumPartButtons[k].GetComponentInChildren<Text>().text = module.name + " " + module.basePrice.ToString();
                MediumPartButtons[k].onClick.RemoveAllListeners();
                PlaneModule tempModule = module;
                int tempK = k;
                MediumPartButtons[k].onClick.AddListener(() => designer.AddModuleToUnlocked(tempModule));
                MediumPartButtons[k].onClick.AddListener(() => MediumPartButtons[tempK].gameObject.SetActive(false));
                MediumPartButtons[k].onClick.AddListener(() => UnlockableModules.Remove(tempModule));

                k++;
            }
        }
        while (k < MediumPartButtons.Count)
        {
            MediumPartButtons[k].gameObject.SetActive(false);
            k++;
        }
        foreach(Button butt in HugePartButtons)
        {
            butt.gameObject.SetActive(false);
        }
    }
    

    public void CheckAvailableUpgrades()
    {
        float engineCost=10000;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == PartType.Engine)
            {
                if (module.basePrice < engineCost)
                {
                    engineCost = module.basePrice;
                }
     
            }
        }
        if (engineCredits >= engineCost)
        {
            engineButton.interactable = true;
        }
        else
        {
            engineButton.interactable = false;
        }


        float wingsCost = 10000;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == PartType.Wing)
            {
                if (module.basePrice < wingsCost)
                {
                    wingsCost = module.basePrice;
                }
            }
        }
        if (wingsCredits >= wingsCost)
        {
            wingsButton.interactable = true;
        }
        else
        {
            wingsButton.interactable = false;
        }
        float landingCost = 10000;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == PartType.LandingGear)
            {
                if (module.basePrice < landingCost)
                {
                    landingCost = module.basePrice;
                }


            }
        }
        if (landingCredits >= landingCost)
        {
            landingButton.interactable = true;
        }
        else
        {
            landingButton.interactable = false;
        }

        float tailCost = 10000;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == PartType.Tail)
            {
                if (module.basePrice < tailCost)
                {
                    tailCost = module.basePrice;
                }


            }
        }
        if (tailCredits >= tailCost)
        {
            tailButton.interactable = true;
        }
        else
        {
            tailButton.interactable = false;
        }

        float cockpitCost = 10000;
        foreach (PlaneModule module in UnlockableModules)
        {
            if (module.partType == PartType.Cockpit)
            {
                if (module.basePrice < cockpitCost)
                {
                    cockpitCost = module.basePrice;
                }


            }
        }
        if (cockpitCredits >= cockpitCost)
        {
            cockpitButton.interactable = true;
        }
        else
        {
            cockpitButton.interactable = false;
        }
    }


    public void UpdateBudgets(Slider activeSlider)
    {

        engineText.text = engineSlider.value.ToString();
        cockpitText.text = cockpitSlider.value.ToString();
        wingsText.text = wingsSlider.value.ToString();
        landingText.text = landingSlider.value.ToString();
        tailText.text = tailSlider.value.ToString();
        cabinText.text = cabinSlider.value.ToString();
        fuselageText.text = fuselageSlider.value.ToString();
    }
    public void OpenPopUpWindow()
    {
        popUpWindow.SetActive(true);
    }
    public void ClosePopUpWindow()
    {
        popUpWindow.SetActive(false);
    }
    private void ReturnUnlockedBudget()
    {
       
        if (!engineLock)
        {
            remainingBudget += engineBudget;
            engineBudget = 0;
        }
        if (!cockpitLock)
        {
            remainingBudget += cockpitBudget;
            cockpitBudget = 0;
        }
        if (!wingsLock)
        {
            Debug.Log("moi");
            remainingBudget += wingsBudget;
            wingsBudget = 0;
        }
        if (!landingLock)
        {
            remainingBudget += landingBudget;
            landingBudget = 0;
        }
        if (!tailLock)
        {
            remainingBudget += tailBudget;
            tailBudget = 0;

        }
        if (!cabinLock)
        {
            remainingBudget += cabinBudget;
            cabinBudget = 0;
        }
        if (!fuselageLock)
        {
            remainingBudget += fuselageBudget;
            fuselageBudget = 0;
        }

    }
    public void OnDrag(Slider activeSlider)
    {
        ReturnUnlockedBudget();
        Debug.Log(remainingBudget);
        if (activeSlider == engineSlider)
        {
            engineLock = true;
        }
        if (activeSlider == cockpitSlider)           
        {
            cockpitLock = true;
        }

        if (activeSlider == wingsSlider)
        {
            wingsLock = true;

        }
        if (activeSlider == landingSlider)
        {
            landingLock = true;
        }
        if (activeSlider == tailSlider)
        {
            tailLock = true;
        }
        if (activeSlider == cabinSlider)
        {
            cabinLock = true;
        }
        if (activeSlider == fuselageSlider)
        {
            fuselageLock = true;
        }
    }
    public void EndDrag(Slider activeSlider)
    {

        if (activeSlider == engineSlider)
        {
            if (engineSlider.value > remainingBudget)
            {
                engineSlider.value = remainingBudget;
            }
            engineBudget = engineSlider.value;
            remainingBudget = remainingBudget - engineBudget;

           
        }
        if (activeSlider == cockpitSlider)
        {
            if (cockpitSlider.value > remainingBudget)
            {
                cockpitSlider.value = remainingBudget;
            }
            cockpitBudget = cockpitSlider.value;
            remainingBudget = remainingBudget - cockpitBudget;

         
        }

        if (activeSlider == wingsSlider)
        {
            if (wingsSlider.value > remainingBudget)
            {
                wingsSlider.value = remainingBudget;
            }
            wingsBudget = wingsSlider.value;
            remainingBudget = remainingBudget - wingsBudget;

           

        }
        if (activeSlider == landingSlider)
        {
            if (landingSlider.value > remainingBudget)
            {
                landingSlider.value = remainingBudget;
            }
            landingBudget = landingSlider.value;
            remainingBudget = remainingBudget - landingBudget;

        }
        if (activeSlider == tailSlider)
        {
            if (tailSlider.value > remainingBudget)
            {
                tailSlider.value = remainingBudget;
            }
            tailBudget = tailSlider.value;
            remainingBudget = remainingBudget - tailBudget;

           
        }
        if (activeSlider == cabinSlider)
        {
            if (cabinSlider.value > remainingBudget)
            {
                cabinSlider.value = remainingBudget;
            }
            cabinBudget = cabinSlider.value;
            remainingBudget = remainingBudget - cabinBudget;

        
          
        }
        if (activeSlider == fuselageSlider)
        {
            if (fuselageSlider.value > remainingBudget)
            {
                fuselageSlider.value = remainingBudget;
            }
            fuselageBudget = fuselageSlider.value;
            remainingBudget = remainingBudget - fuselageBudget;

           
        }
       
       SplitRemaining();
        CheckLocks();
        Debug.Log(remainingBudget);
    }

    public void CheckLocks()
    {
        if (engineLock && engineImage.sprite == sliderUnlock)
        {
            engineLock = false;
        }
        if (cockpitLock && cockpitImage.sprite == sliderUnlock)
        {
            cockpitLock = false;
        }
        if (wingsLock && wingsImage.sprite == sliderUnlock)
        {
            wingsLock = false;
        }
        if (landingLock && landingImage.sprite == sliderUnlock)
        {
            landingLock = false;
        }
        if (tailLock && tailImage.sprite == sliderUnlock)
        {
            tailLock = false;
        }
        if (cabinLock && cabinImage.sprite == sliderUnlock)
        {
            cabinLock = false;
        }
        if (fuselageLock && fuselageImage.sprite == sliderUnlock)
        {
            fuselageLock = false;
        }

    }

    public void ReadBudget()
    {
        float oldBudget = researchBudget;
        float newBudget;
        float tempBudget;

        try
        {
            newBudget = Int32.Parse(budgetInput.text);
            if ((int)newBudget <=gamemanager.CompanyFunds) {
                tempBudget = newBudget - oldBudget;
                if (tempBudget >= 0)
                {
                    researchBudget = newBudget;
                    remainingBudget += tempBudget;
                }
                else if (newBudget >0 &&newBudget <10)
                {
                    if (newBudget  %2 == 0) {
                        tempBudget = newBudget;
                        researchBudget = newBudget;
                        remainingBudget = 0;
                        wingsBudget = tempBudget / 2;
                        landingBudget = tempBudget / 4;
                        tailBudget = tempBudget / 4;
                        wingsSlider.value = tempBudget / 2;
                        landingSlider.value = tempBudget / 4;
                        tailSlider.value = tempBudget / 4;
                        engineBudget = 0;
                        cockpitBudget = 0;
                        cabinBudget = 0;
                        fuselageBudget = 0;
                        engineSlider.value = 0;
                        cockpitSlider.value = 0;
                        cabinSlider.value = 0;
                        fuselageSlider.value = 0;
                        if (newBudget == 2)
                        {
                            landingBudget += 1;
                            landingSlider.value += 1;

                        }
                        if (newBudget == 6)
                        {
                            landingBudget += 1;
                            landingSlider.value += 1;

                        }
                    }
                    else
                    {
                      
                        tempBudget = newBudget;
                        researchBudget = newBudget;
                        remainingBudget = 0;
                        wingsBudget = tempBudget / 2 + 1;
                        landingBudget = tempBudget / 4;
                        tailBudget = tempBudget / 4;
                        wingsSlider.value = tempBudget / 2 + 1;
                        landingSlider.value = tempBudget / 4;
                        tailSlider.value = tempBudget / 4;

                        engineBudget = 0;
                        cockpitBudget = 0;
                        cabinBudget = 0;
                        fuselageBudget = 0;
                        engineSlider.value = 0;
                        cockpitSlider.value = 0;
                        cabinSlider.value = 0;
                        fuselageSlider.value = 0;
                        if (newBudget == 3)
                        {
                            landingBudget = 1;
                            landingSlider.value = 1;
                            tailBudget = 0;
                            tailSlider.value = 0;
                        }
                        else    if (newBudget == 7 )
                        {
                            landingBudget += 1;
                            landingSlider.value += 1;

                        }
                    }

                }
                else
                {
                    researchBudget = newBudget;
                    remainingBudget = newBudget;
                    engineBudget = 0;
                    cockpitBudget = 0;
                    wingsBudget = 0;
                    landingBudget = 0;
                    tailBudget = 0;
                    cabinBudget = 0;
                    fuselageBudget = 0;

                    engineSlider.value = 0;
                    cockpitSlider.value = 0;
                    wingsSlider.value = 0;
                    landingSlider.value = 0;
                    tailSlider.value = 0;
                    cabinSlider.value = 0;
                    fuselageSlider.value = 0;
                }
                budgetInput.placeholder.GetComponent<Text>().text = researchBudget + "$";
                budgetInput.text = "";
                engineSlider.maxValue = researchBudget;
                cockpitSlider.maxValue = researchBudget;
                wingsSlider.maxValue = researchBudget;
                landingSlider.maxValue = researchBudget;
                tailSlider.maxValue = researchBudget;
                cabinSlider.maxValue = researchBudget;
                fuselageSlider.maxValue = researchBudget;
                UpdateTexts();

            }else
            {
                budgetInput.placeholder.GetComponent<Text>().text = researchBudget + "$";
                budgetInput.text = "";
            }
        }
        catch (FormatException e)
        {
            Debug.Log(e.Message);
        }

    }
    public void SplitRemaining()
    {
        ReturnUnlockedBudget();
        Debug.Log(remainingBudget);
        float sharedBudget =0;
        float objectsFree = 0;
        float tempRemainder;
        for (int i = 0; i < 2; i++)
        {
            if (!engineLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                
                if (i == 1)
                {
                    engineBudget = Mathf.Floor(remainingBudget / objectsFree);
                    engineSlider.value = engineBudget;
                    sharedBudget += engineBudget;
                   
                }
            }
           if (!cockpitLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                if (i == 1)
                {
                    cockpitBudget = Mathf.Floor(remainingBudget / objectsFree);
                    cockpitSlider.value = cockpitBudget;
                    sharedBudget += cockpitBudget;
                }
            }
            if (!wingsLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                if (i == 1)
                {
                    wingsBudget = Mathf.Floor(remainingBudget / objectsFree);
                    wingsSlider.value = wingsBudget;
                    sharedBudget += wingsBudget;
                }
            }
            if (!landingLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                if (i == 1)
                {
                   
                    landingBudget = Mathf.Floor(remainingBudget / objectsFree);
                    landingSlider.value = landingBudget;
                    sharedBudget += landingBudget;
                }
            }
           if (!tailLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                if (i == 1)
                {
                   tailBudget = Mathf.Floor(remainingBudget / objectsFree);
                    tailSlider.value = tailBudget;
                    sharedBudget += tailBudget;
                }
            }
             if (!cabinLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                if (i == 1)
                {
                    cabinBudget = Mathf.Floor(remainingBudget / objectsFree);
                    cabinSlider.value = cabinBudget;
                    sharedBudget += cabinBudget;
                }
            }
           if (!fuselageLock)
            {
                if (i == 0)
                {
                    objectsFree++;
                }
                if (i == 1)
                {
                    fuselageBudget = Mathf.Floor(remainingBudget / objectsFree);
                    fuselageSlider.value = fuselageBudget;
                    sharedBudget += fuselageBudget;
                }
            }
            
            }
           
        
             tempRemainder = remainingBudget % objectsFree;
             Debug.Log("Jäännös on: " + tempRemainder);
            Debug.Log("remain" + remainingBudget);
            remainingBudget -= sharedBudget;
            Debug.Log("remain" + remainingBudget);
            if (remainingBudget < 0)
            {
                remainingBudget = 0;
            }
        if (tempRemainder > 0)
        {
            Debug.Log("remain" + remainingBudget);
            if (!wingsLock)
            {
                wingsBudget += tempRemainder;
                wingsSlider.value += tempRemainder;
                tempRemainder = 0;
            }
            else if (!landingLock)
            {
                landingBudget += tempRemainder;
                landingSlider.value += tempRemainder;
                tempRemainder = 0;
            }
            else if (!tailLock)
            {
                tailBudget += tempRemainder;
                tailSlider.value += tempRemainder;
                tempRemainder = 0;
            }
            else
            {
                engineBudget += tempRemainder;
                engineSlider.value += tempRemainder;
                tempRemainder = 0;
            }
          
            Debug.Log("remain" + remainingBudget);
            remainingBudget = 0;

        }
        UpdateTexts();
    }
    public void UpdateTexts()
    {
        engineText.text = (int)engineBudget + "$ / Month";
        cockpitText.text = (int)cockpitBudget + "$ / Month";
        wingsText.text = (int)wingsBudget + "$ / Month";
        landingText.text = (int)landingBudget + "$ / Month";
        tailText.text = (int)tailBudget + "$ / Month";
        cabinText.text = (int)cabinBudget + "$ / Month";
       fuselageText.text = (int)fuselageBudget + "$ / Month";
    }
	public void ChangeLock(string name)
    {
        Debug.Log(remainingBudget);
        if (name == "engine")
        {
            if (engineImage.sprite == sliderUnlock)
            {
                engineBudget = engineSlider.value;
                engineImage.sprite = sliderLock;
                engineSlider.interactable = false;
                engineLock = true;
            }
            else
            {
                engineImage.sprite = sliderUnlock;
                engineSlider.interactable = true;
                engineLock = false;
            }


        }
        else if (name == "cockpit")
        {
            if (cockpitImage.sprite == sliderUnlock)
            {
                cockpitImage.sprite = sliderLock;
                cockpitSlider.interactable = false;
                cockpitLock = true;
            }
            else
            {
                cockpitImage.sprite = sliderUnlock;
                cockpitSlider.interactable = true;
                cockpitLock = false;
            }

        }
        else if (name == "wings")
        {
            if (wingsImage.sprite == sliderUnlock)
            {
                
                wingsImage.sprite = sliderLock;
                wingsSlider.interactable = false;
                wingsLock = true;
            }
            else
            {
                wingsImage.sprite = sliderUnlock;
                wingsSlider.interactable = true;
                wingsLock = false;
            }
        }
        else if (name == "tail")
        {
            if (tailImage.sprite == sliderUnlock)
            {
                tailImage.sprite = sliderLock;
                tailSlider.interactable = false;
                tailLock = true;
            }
            else
            {
                tailImage.sprite = sliderUnlock;
                tailSlider.interactable = true;
                tailLock = false;
            }
        }
        else if (name == "landing")
        {
            if (landingImage.sprite == sliderUnlock)
            {
                landingImage.sprite = sliderLock;
                landingSlider.interactable = false;
                landingLock = true;
            }
            else
            {
                landingImage.sprite = sliderUnlock;
                landingSlider.interactable = true;
                landingLock = false;
            }
        }
        else if (name == "fuselage")
        {
            if (fuselageImage.sprite == sliderUnlock)
            {
                fuselageImage.sprite = sliderLock;
                fuselageSlider.interactable = false;
                fuselageLock = true;
            }
            else
            {
                fuselageImage.sprite = sliderUnlock;
                fuselageSlider.interactable = true;
                fuselageLock = false;
            }
        }
        else if (name == "cabin")
        {
            if (cabinImage.sprite == sliderUnlock)
            {
                cabinImage.sprite = sliderLock;
                cabinSlider.interactable = false;
                cabinLock = true;
            }
            else
            {
                cabinImage.sprite = sliderUnlock;
                cabinSlider.interactable = true;
                cabinLock = false;
            }
        }
        
        }
    
    public void NewMonth()
    {
        engineCredits += engineBudget;
        cockpitCredits += cockpitBudget;
       wingsCredits += wingsBudget;
        landingCredits +=landingBudget;
        tailCredits += tailBudget;
        cabinCredits += cabinBudget;
       fuselageCredits += fuselageBudget;
        CheckAvailableUpgrades();

    }


	// Update is called once per frame
	public void Update () {
      //  Debug.Log(remainingBudget);
	}
}
