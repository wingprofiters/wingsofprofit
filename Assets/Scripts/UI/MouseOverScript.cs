﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Script for map screen mouseover / click effects

public class MouseOverScript : MonoBehaviour
{
    public bool MouseOverEnabled = true;
    private MarketView marketView;
    private GameObject lastHit;

    void Start()
    {
        marketView = FindObjectOfType<MarketView>();
    }


    void Update()
    {
        if (!MouseOverEnabled) return;
        //int MapMask = 1 << 8; // Map layer
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (hit.collider != null)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                marketView.AreaClicked(hit.transform.gameObject);
                return;
            }
            if (lastHit == hit.transform.gameObject) return;
            else
            {
                lastHit = hit.transform.gameObject;
                marketView.MouseOverArea(lastHit);
            }
            //Debug.Log("Target Position: " + hit.collider.gameObject.transform.position);
            //hit.transform.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
        {
            lastHit = null;
            marketView.MouseOverArea(null);
        }

    }

}