﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchSelector : MonoBehaviour {
    public GameObject SelectorContainer;
    public GameObject leftMount;
    public GameObject rightMount;

    public Vector2 SelectorVector;
    public Vector2 rightVector;
    public Vector2 leftVector;
    public Vector2 nextVector;

    public float speed;
    private float startTime;
    private float journeyLength;
    private bool canMove = true;

    public bool moveToRightB = false;
    public bool moveToLeftB = false;


    // Use this for initialization
   public  void Start () {
        
        SelectorVector = SelectorContainer.transform.position;
        rightVector = rightMount.transform.position;
        leftVector = leftMount.transform.position;
        startTime = Time.time;
        journeyLength = Vector2.Distance(SelectorContainer.transform.position, leftMount.transform.position);
        speed = 1500f;
    }
    public void MoveToLeft()
    {
      if (canMove)
        {      
            nextVector = rightVector;        
            startTime = Time.time;
            moveToRightB = true;
            canMove = false;
        }
    }

    public void MoveToRight()
    {
        if (canMove)
        {          
            nextVector = leftVector;
            startTime = Time.time;
            moveToLeftB = true;
            canMove = false;
        }
    }

    // Update is called once per frame
    public void Update () {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        if ( moveToRightB)
        {
            SelectorContainer.transform.position = Vector2.Lerp(SelectorVector, nextVector, fracJourney);    
            if (SelectorContainer.transform.position.x >= nextVector.x)
            {
                SelectorVector = nextVector;
                moveToRightB = false;
                canMove = true;
            }
        }
        if ( moveToLeftB)
        {
            SelectorContainer.transform.position = Vector2.Lerp(SelectorVector, nextVector, fracJourney);
            if (SelectorContainer.transform.position.x-5 <= nextVector.x)
            {
                SelectorVector = nextVector;
                moveToLeftB = false;
                canMove = true;
            }
        }
    }
}
