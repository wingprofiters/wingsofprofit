﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotorRotation : MonoBehaviour {
    public Image rotor;
    public double rotorSpeed;
    public float potkuriSpeed = 80;
    public float lastSpeed = 0;
    private float launchDelay;
    private float boost;

    public 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        rotor.rectTransform.Rotate(0, 0, Time.deltaTime * potkuriSpeed);
    }
}
