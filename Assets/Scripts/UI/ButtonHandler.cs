﻿
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour {

    public Image engine;
    public Image cockpit;
    public Image wings;
    public Image landing;
    public Image tail;
    public Sprite buttonActive;
    public Sprite buttonOff;
    public Image last;
 


    // Use this for initialization
    public void Start () {

        last = cockpit;

    }
    public void ActivateButton(string name)
    {
        if (name == "engine")
        {
            last.sprite = buttonOff;
            engine.sprite = buttonActive;
            last = engine;

        }
        else if(name == "cockpit")
        {
            last.sprite = buttonOff;
            cockpit.sprite = buttonActive;
            last = cockpit;

        }
        else if(name == "wings")
        {
            last.sprite = buttonOff;
            wings.sprite = buttonActive;
            last = wings;
        }
        else if(name == "tail")
        {
            last.sprite = buttonOff;
            tail.sprite = buttonActive;
            last = tail;
        }
        else if(name == "landing")
        {
            last.sprite = buttonOff;
            landing.sprite = buttonActive;
            last = landing;
        }
    }
}
