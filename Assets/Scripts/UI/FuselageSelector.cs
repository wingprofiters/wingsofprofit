﻿using UnityEngine;
public class FuselageSelector : MonoBehaviour {

    public GameObject mediumFuselage;
    public GameObject smallFuselage;
    public GameObject hugeFuselage;

    public GameObject active;
    public GameObject leftHolder;
    public GameObject rightHolder;
    private GameObject tempObj;

    private Vector2 activeVector;
    private Vector2 leftVector;
    private Vector2 rightVector;

    private bool moveFromRightB = false;
    private bool moveToRightB=false;

    public bool moveFromLeftB= false;
    public bool moveToLeftB=false;

    public float speed;
    private float startTime;
    private float journeyLength;

    private bool canMove = true;

    // Use this for initialization
    public void Start () {
        
        active = mediumFuselage;
        leftHolder = smallFuselage;
        rightHolder = hugeFuselage;
        activeVector = active.transform.position;
        leftVector = leftHolder.transform.position;
        rightVector = rightHolder.transform.position;

        startTime = Time.time;
        journeyLength = Vector2.Distance(leftHolder.transform.position, active.transform.position);
        speed = 1100f;
    }
	
    public void MoveToLeft()
    {
        if (canMove)
        {
            startTime = Time.time;
            moveFromRightB = true;
            moveToLeftB = true;
            canMove = false;
        }
    }

    public void MoveToRight()
    {
        if (canMove)
        {
            startTime = Time.time;
        moveFromLeftB = true;
        moveToRightB = true;
            canMove = false;
        }
    }
	// Update is called once per frame
	public void Update () {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        if (moveFromLeftB && moveToRightB)
        {
            leftHolder.transform.position = Vector2.Lerp(leftVector, activeVector, fracJourney);
            active.transform.position = Vector2.Lerp(activeVector, rightVector, fracJourney);
            if (active.transform.position.x+1 >= rightVector.x)
            {
                moveFromLeftB = false;
                moveToRightB = false;
                tempObj = rightHolder;
                rightHolder = active;
                active = leftHolder;
                leftHolder = tempObj;
                leftHolder.transform.position = leftVector;
                canMove = true;


            }
        }
        if (moveFromRightB && moveToLeftB)
        {
            rightHolder.transform.position = Vector2.Lerp(rightVector, activeVector, fracJourney);
            active.transform.position = Vector2.Lerp(activeVector, leftVector, fracJourney);
            if (active.transform.position.x-1 <= leftVector.x)
            {
                moveToRightB = false;
                moveToLeftB = false;
                tempObj = leftHolder;
                leftHolder = active;
                active = rightHolder;
                rightHolder = tempObj;
                rightHolder.transform.position = rightVector;
                canMove = true;
            }
        }

	}
}
