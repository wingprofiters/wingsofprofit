﻿using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour {

    private Image designTab;
    private Image marketingTab;
    private Image researchTab;
    private Image administrationTab;
    public Sprite designOn;
    public Sprite designOff;
    public Sprite researchOn;
    public Sprite researchOff;
    public Sprite administrationOn;
    public Sprite administrationOff;
    public Sprite marketingOn;
    public Sprite marketingOff;

    public Sprite marketingExplain;
    public Sprite designExplain;
    public Sprite researchExplain;
    public Sprite administrationExplain;

    public Image tabExplain;

    // Use this for initialization
    public void Start () {
        designTab = GameObject.Find("ButtonDesign").GetComponent<Image>();
        marketingTab = GameObject.Find("ButtonMarketing").GetComponent<Image>();
        researchTab = GameObject.Find("ButtonResearch").GetComponent<Image>();
        administrationTab = GameObject.Find("ButtonAdministration").GetComponent<Image>();
        tabExplain.sprite = designExplain;

    }
	public void OpenMarketing()
    {
        designTab.sprite = designOff;
        administrationTab.sprite = administrationOff;
        researchTab.sprite = researchOff;
        marketingTab.sprite = marketingOn;
        tabExplain.sprite = marketingExplain;
       // marketingTab.
       // designTab.useGUILayout = 
        // GUI.depth = 1;



    }

    public void OpenDesign()
    {
        
        administrationTab.sprite = administrationOff;
        researchTab.sprite = researchOff;
        marketingTab.sprite = marketingOff;
        designTab.sprite = designOn;
        tabExplain.sprite = designExplain;
    }
    public void OpenAdministration()
    {
        designTab.sprite = designOff;
        researchTab.sprite = researchOff;
        marketingTab.sprite = marketingOff;
        administrationTab.sprite = administrationOn;
        tabExplain.sprite = administrationExplain;
    }
    public void OpenResearch()
    {
        designTab.sprite = designOff;
        marketingTab.sprite = marketingOff;
        administrationTab.sprite = administrationOff;
        researchTab.sprite = researchOn;
        tabExplain.sprite = researchExplain;
    }


   
    // Update is called once per frame
    void Update () {
		
	}
}
