﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Manage the "esacape" menu buttons and actions
/// </summary>
public class EscapeMenuScript : MonoBehaviour {

    public GameObject EscapeMenuContainer;
    public GameObject SettingsWindowContainer;
    public GameObject SaveGameConfirmationContainer;
    public GameObject QuitGameFolder;
    public GameObject ToMainMenuFolder;
    public GameObject GameSaved;
    public Image GameSavedImage;

    public bool SavedGame = false;
    public float imageFadeTime;
    public float fader;
    public Color c;

	// Use this for initialization
	void Start () {
        EscapeMenuContainer.SetActive(false);
        SettingsWindowContainer.SetActive(false);
        ToMainMenuFolder.SetActive(false);
        QuitGameFolder.SetActive(false);
        
        c = GameSavedImage.color;
        c.a = 1;
        fader = 1.5f;
        GameSavedImage.color = c;
        GameSaved.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (SavedGame)
        {
            GameSaved.SetActive(true);
            c.a = fader;
            fader = fader - Time.deltaTime;
            GameSavedImage.color = c;
            if (c.a <= 0)
            {
                GameSaved.SetActive(false);
                fader = 1.5f;
                SavedGame = false;
            }
        }
        
        

        if (Input.GetKeyUp("escape"))
        {
            
            if(EscapeMenuContainer.activeInHierarchy== false)
            {
                EscapeMenuContainer.SetActive(true);
            }
            else
            {
                EscapeMenuContainer.SetActive(false);
            }
        }
    }
    public void CloseMenu()
    {
        EscapeMenuContainer.SetActive(false);
    }
    public void OpenMenu()
    {

    }
    public void OpenSettingsWindow()
    {
        SettingsWindowContainer.SetActive(true);
    }
    public void CloseSettingsWindow()
    {
        SettingsWindowContainer.SetActive(false);
    }

   public void OpenToMainMenu()
    {
        ToMainMenuFolder.SetActive(true);
    }
    public void CloseToMainMenu()
    {
        ToMainMenuFolder.SetActive(false);
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void SaveGame()
    {
        Debug.Log("moi");
       // GameSaved.SetActive(true);
        SavedGame = true;
    }

    public void OpenQuit()
    {
        QuitGameFolder.SetActive(true);
    }
    public void CloseQuit()
    {
        QuitGameFolder.SetActive(false);
    }
    public void QuitGame()
    {
        // Are you sure prompt?
        Application.Quit();
    }
}
