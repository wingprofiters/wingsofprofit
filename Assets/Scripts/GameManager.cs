﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Manage main game state, manage views

public class GameManager : MonoBehaviour {

    public const int START_YEAR = 1960;
    public int GameMonth = 0;
    public int CompanyFunds = 0;
    public int RevenueLastMonth = 0;
    public int IncomeLastMonth = 0;
    public List<int> PlaneMarketAddCosts;

    public Button MarketingButton;
    public Button ResearchButton;
    public Button DesignButton;
    public Button AdministrationButton;

    public GameObject MarketingContainer;
    public GameObject ResearchContainer;
    public GameObject AdministrationContainer;
    public GameObject DesignContainer;
    public GameObject MapTextures;

    private PlaneDesigner planeDesignerScript;
    public MarketView marketView;
    public ResearchBudget researchBudget;
    private AdministrationView administrationView;

    private GameObject activeContainer;

    public Text TabExplanation;
    public Text CompanyFundsText;
    public Text DateText;
    private Text companyName;

    public readonly string[] monthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    // Use this for initialization
    void Start ()
    {
        PlaneMarketAddCosts = new List<int>();
        CompanyFundsText.text = CompanyFunds.ToString("C0"); // Format to local currency
        DateText.text = monthNames[GameMonth % 12] + " " + (1960 + GameMonth / 12);
        planeDesignerScript = FindObjectOfType<PlaneDesigner>();
        administrationView = FindObjectOfType<AdministrationView>();
        marketView = FindObjectOfType<MarketView>();
        researchBudget = FindObjectOfType<ResearchBudget>();
        companyName = GameObject.Find("TextCompanyName").GetComponent<Text>();
        companyName.text = PlayerPrefs.GetString("ActivePlayer");
        // if new game, add some modules to unlocked list
        if (planeDesignerScript && GameMonth == 0)
        {            
            planeDesignerScript.AddModulesToDesigner(ModuleCreator.AllModules());
            planeDesignerScript.CreatePartDictionary();
            planeDesignerScript.AddModulesToUnlocked(ModuleCreator.StarterModules());
            //Debug.Log("Unlockable modules: " + ModuleCreator.UnlockableModules().Count);
        }
        DesignContainer.SetActive(true);
        MarketingContainer.SetActive(false);
        ResearchContainer.SetActive(false);
        AdministrationContainer.SetActive(false);
        MapTextures.SetActive(false);
        activeContainer = DesignContainer;
    }
	
	// Update is called once per frame
	void Update ()
    {
#if UNITY_EDITOR
        // DEBUG
        if (Input.GetKeyDown(KeyCode.M))
        {
            AdvanceTime();
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            planeDesignerScript.AddModulesToUnlocked(ModuleCreator.UnlockableModules());
        }
#endif
        if (Input.GetButtonDown("Cancel")) // Esc
        {
            // Open/Close esc menu
        }
    }

    public PlaneDesigner GetDesignerScript()
    {
        return planeDesignerScript;
    }

    public void MarketClicked()
    {
        TabExplanation.text = "MARKET";
        if (activeContainer == MarketingContainer) return;
        activeContainer.SetActive(false);
        MarketingContainer.SetActive(true);
        MapTextures.SetActive(true);
        activeContainer = MarketingContainer;
        administrationView.MarketingTabVisited = true;
    }
    public void ResearchClicked()
    {
        TabExplanation.text = "RESEARCH";
        if (activeContainer == ResearchContainer) return;
        activeContainer.SetActive(false);
        ResearchContainer.SetActive(true);
        activeContainer = ResearchContainer;
        MapTextures.SetActive(false);
        administrationView.ResearchTabVisited = true;
    }
    public void DesignClicked()
    {
        TabExplanation.text = "DESIGN";
        if (activeContainer == DesignContainer) return;
        activeContainer.SetActive(false);
        DesignContainer.SetActive(true);
        activeContainer = DesignContainer;
        MapTextures.SetActive(false);
        administrationView.DesignTabVisited = true;
    }
    public void AdministrationClicked()
    {
        TabExplanation.text = "ADMINISTRATION";
        if (activeContainer == AdministrationContainer) return;
        activeContainer.SetActive(false);
        AdministrationContainer.SetActive(true);
        activeContainer = AdministrationContainer;
        MapTextures.SetActive(false);
    }
    public void GoBackToDesignStart()
    {
        AdministrationClicked();
        DesignClicked();
    }
    public void AdvanceTime() // Called at the end of month/turn
    {
        // Designer?
        GameMonth++;
        // Market
        DateText.text = monthNames[GameMonth % 12] + " " + (START_YEAR + GameMonth / 12);
        int budgetchange = marketView.AdvanceTime(); // Market areas have to have been found, market view must be opened once

        IncomeLastMonth = budgetchange;
      
        // Research
        researchBudget.NewMonth();
        UpdateFunds(budgetchange - (int)researchBudget.researchBudget);
    }

    public void UpdatePlaneMarketAdditionCosts(int cost)
    {
        PlaneMarketAddCosts.Add(cost); // Reset every month?
        CompanyFunds -= cost;
        CompanyFundsText.text = CompanyFunds.ToString("C0"); // Format to local currency
        if (CompanyFunds < 0)
        {
            CompanyFundsText.color = Color.red;
        }
        else
        {
            CompanyFundsText.color = Color.white;
        }
    }

    public void UpdateFunds(int change)
    {
        RevenueLastMonth = change;
        CompanyFunds += change;
      
        CompanyFundsText.text = CompanyFunds.ToString("C0"); // Format to local currency
        if (CompanyFunds < 0)
        {
            CompanyFundsText.color = Color.red;
        }
        else
        {
            CompanyFundsText.color = Color.white;
        }
    }
}

public enum MainTab
{
    Design,
    Marketing,
    Research,
    Administration,
}