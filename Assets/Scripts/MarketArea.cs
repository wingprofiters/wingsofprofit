﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Single market area in the game

public class MarketArea : MonoBehaviour {
    // Funds allocated for marketing?
    // Designs for sale?
    // What is the market area interested in? Fast/Cheap/Long Range?
    // Sales last month, profit/loss?
    // Brand recognition?

    public string AreaName;
    public int MarketSizeCap; // Max amount of planes you can sell?

    // What this market area wants
    // 0 for not importat. One should at least be important
    public float WantedSpeed;
    public int WantedRange;
    public int WantedPrice;

    public int MarketingBudget;
    public float Fame;
    public PlaneDesign[] DesignsForSale; // One for enemy companies too?
    public int DesignsOnSaleNumber;
    // List for how much you ask?
    public int ProfitLastMonth = 0;

    public struct DesignDeviation
    {
        public float Deviation;
        public PlaneDesign Design;
    }

	// Use this for initialization
	void Start () {
        DesignsForSale = new PlaneDesign[3]; // 3 slots, adjust for more!
        DesignsOnSaleNumber = 0;
        PlaneDesign noDesn = new PlaneDesign();
        noDesn.FuselageType = Fuselage.None;
        noDesn.Name = "NODESIGN";
        for (int i = 0; i < 3; i++)
        {
            DesignsForSale[i] = noDesn;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool AddDesignToMarketSlot(int slot, PlaneDesign design)
    {
        //int pos = Array.IndexOf(DesignsForSale, design);
        bool isForSale = isDesignOnMarket(design);
        if (isForSale)
        {
            // Error?
            Debug.Log("Design already on market: " + design.Name);
            return false;
        }
        else if (slot > DesignsForSale.Length)
        {
            Debug.Log("Design slot too high: " + slot);
            return false;
        }
        else
        {
            DesignsForSale[slot] = design;
            DesignsOnSaleNumber++;
            return true;
        }
    }

    private bool isDesignOnMarket(PlaneDesign pd)
    {
        for (int i = 0; i < 3; i++)
        {
            if (DesignsForSale[i] == pd) // Better check?
                return true;
        }
        return false;
    }

    public void RemoveDesignFromMarket(int slot)
    {
        PlaneDesign noDes = new PlaneDesign();
        noDes.FuselageType = Fuselage.None;
        DesignsForSale[slot] = noDes;
        DesignsOnSaleNumber--;
        // Remove price input field numbers
    }

    public int AdvanceTime() // Called at the end of month
    {
        Fame += Mathf.Sqrt(MarketingBudget);
        Fame = Fame * 0.9f; // Ok?
        // Calculate sales. Random events here?
        ProfitLastMonth = -MarketingBudget; // is this ok?
        if (DesignsForSale[0].FuselageType == Fuselage.None && DesignsForSale[1].FuselageType == Fuselage.None &&
            DesignsForSale[2].FuselageType == Fuselage.None)
        {
            // Plane price effect on sales
            //Mathf.Min(0, MarketSizeCap + ( WantedPrice - DesignPrice)/2f);
            //Debug.Log(ProfitLastMonth);
            return ProfitLastMonth;
        }
        else
        {
            // Calculate designs deviation from wanted stats
            List<DesignDeviation> deviations = new List<DesignDeviation>();
            foreach (PlaneDesign design in DesignsForSale)
            {
                if (design.FuselageType == Fuselage.None)
                {
                    continue;
                }
                DesignDeviation ddev;
                ddev.Deviation = 0;
                ddev.Design = design;
                float importatCategories = 0;
                if (WantedSpeed > 0)
                {
                    ddev.Deviation = Mathf.Abs(WantedSpeed - (design.Weight / design.Speed) / WantedSpeed);
                    importatCategories++;
                }
                if (WantedRange > 0)
                {
                    ddev.Deviation += Mathf.Abs((WantedRange - design.Range) / WantedRange);
                    importatCategories++;
                }
                importatCategories = (importatCategories == 0) ? 1 : importatCategories; // Set importantcategories to 1 if it's 0 to avoid division by zero
                ddev.Deviation = ddev.Deviation / importatCategories;
                //Debug.Log(AreaName + " - " + ddev.Design.Name + ": " + ddev.Deviation);
                deviations.Add(ddev);
            }
            deviations.Sort((s1, s2) => s1.Deviation.CompareTo(s2.Deviation)); // Smallest to largest, best to worst
            int num = 0;
            foreach (DesignDeviation ddev in deviations)
            {
                float priceVal = 1f - (ddev.Design.PriceAsk / (ddev.Design.ManufactureCost * 1.5f));
                if (priceVal < 0)
                    continue; // No sales
                float randomVariation = 1f + UnityEngine.Random.Range(-0.3f, 0.3f);
                // Good equations for sales go here
                if (WantedPrice > 0)
                {
                    // Price matters
                    ProfitLastMonth += (int)((ddev.Design.PriceAsk - ddev.Design.ManufactureCost) * ((float)MarketSizeCap / (deviations.Count + num)) *
                        Mathf.Sqrt(Fame+1) * (WantedPrice / ddev.Design.PriceAsk) * priceVal * randomVariation);
                    num++;
                }
                else // Price does not matter
                {
                    ProfitLastMonth += (int)((ddev.Design.PriceAsk - ddev.Design.ManufactureCost) * ((float)MarketSizeCap / (deviations.Count + num)) *
                        Mathf.Sqrt(Fame+1) * priceVal * randomVariation);
                    num++;
                }
            }
        }
        return ProfitLastMonth;
    }
}
