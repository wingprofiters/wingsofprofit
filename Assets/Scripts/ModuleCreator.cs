﻿using System.Collections.Generic;
using System.Linq;

public static class ModuleCreator
{
    public static List<PlaneModule> AllModules()
    {
        List<PlaneModule> allModules = new List<PlaneModule>();

        // Tiny modules
        //                  Module          Fuselage        Name                Cost, Weight   Type          (speed/passengers/range)
        allModules.Add(new WingModule(      Fuselage.Tiny, "Low wing",          100f, 100f, WingType.Low));
        allModules.Add(new WingModule(      Fuselage.Tiny, "High wing",         110f, 110f, WingType.High));
        allModules.Add(new WingModule(      Fuselage.Tiny, "Motor wing",        150f, 120f, WingType.MidMotor, 90f, 150f));
        allModules.Add(new WingModule(      Fuselage.Tiny, "Quad motor wing",   220f, 140f, WingType.QuadMotor, 140f, 250f));
        allModules.Add(new WingModule(      Fuselage.Tiny, "Double wing",       140f, 120f, WingType.High));
        allModules.Add(new EngineModule(    Fuselage.Tiny, "Basic propeller",   150f, 50f, EngineType.Propeller, 100f, 100f));
        allModules.Add(new EngineModule(    Fuselage.Tiny, "Fast propeller",    250f, 60f, EngineType.Propeller, 130f, 120f));
        allModules.Add(new EngineModule(    Fuselage.Tiny, "Fastest propeller", 250f, 60f, EngineType.Propeller, 160f, 150f));
        allModules.Add(new EngineModule(    Fuselage.Tiny, "No engine",         30f, 20f, EngineType.NoEngine, 0f, 30f));
        allModules.Add(new CockPitModule(   Fuselage.Tiny, "Basic cockpit",     100f, 100f, CockpitType.Basic, 3));
        allModules.Add(new CockPitModule(   Fuselage.Tiny, "Advanced cockpit",  200f, 80f, CockpitType.Advanced, 4));
        allModules.Add(new CockPitModule(   Fuselage.Tiny, "Open cockpit",      50f, 40f, CockpitType.Open));
        allModules.Add(new LandingGearModule(Fuselage.Tiny, "Fixed landing gear", 50f, 40f, LandingGearType.Fixed));
        allModules.Add(new LandingGearModule(Fuselage.Tiny, "Advanced landing gear", 50f, 40f, LandingGearType.Fixed));
        allModules.Add(new LandingGearModule(Fuselage.Tiny, "Retractable landing gear", 80f, 20f, LandingGearType.Retractable));
        allModules.Add(new TailModule(      Fuselage.Tiny, "Basic tail",        30f, 30f, TailType.Basic));
        allModules.Add(new TailModule(      Fuselage.Tiny, "Aerodynamic tail",  50f, 20f, TailType.Aerodynamic));
        allModules.Add(new TailModule(      Fuselage.Tiny, "Large tail",        90f, 30f, TailType.Basic));

        // Medium modules
        //                  Module          Fuselage        Name                Cost, Weight   Type          (speed/passengers/range)
        allModules.Add(new WingModule(      Fuselage.Medium, "Basic wing",      100f, 100f, WingType.Low));
        allModules.Add(new WingModule(      Fuselage.Medium, "Light wing",      140f, 80f, WingType.Low));
        allModules.Add(new WingModule(      Fuselage.Medium, "Rotor wing",      150f, 120f, WingType.MidMotor, 120f, 150f));
        allModules.Add(new EngineModule(    Fuselage.Medium, "Basic Motor",     150f, 50f, EngineType.Propeller, 150f, 150f));
        allModules.Add(new EngineModule(    Fuselage.Medium, "Fast Motor",      150f, 50f, EngineType.FastPropeller, 220f, 180f));
        allModules.Add(new CockPitModule(   Fuselage.Medium, "Basic Cockpit",   100f, 100f, CockpitType.Basic, 5));
        allModules.Add(new CockPitModule(   Fuselage.Medium, "Modern Cockpit",  180f, 85f,  CockpitType.Advanced, 7));
        allModules.Add(new CockPitModule(   Fuselage.Medium, "Fast Cockpit",    190f, 80f, CockpitType.Advanced, 6));
        allModules.Add(new LandingGearModule(Fuselage.Medium, "Basic Landing gear", 50f, 40f, LandingGearType.Fixed));
        allModules.Add(new LandingGearModule(Fuselage.Medium, "Retractable Landing gear", 50f, 40f, LandingGearType.Retractable));
        allModules.Add(new TailModule(      Fuselage.Medium, "Basic Tail",      30f, 30f, TailType.Basic));
        allModules.Add(new TailModule(      Fuselage.Medium, "Aerodynamic Tail", 60f, 25f, TailType.Aerodynamic));


        return allModules;
    }

    public static List<PlaneModule> StarterModules()
    {
        List<PlaneModule> modules = new List<PlaneModule>();

        modules.Add(new WingModule(Fuselage.Tiny, "Low wing", 100f, 100f, WingType.Low));
        modules.Add(new WingModule(Fuselage.Tiny, "High wing", 110f, 110f, WingType.High));
        modules.Add(new WingModule(Fuselage.Tiny, "Motor wing", 150f, 120f, WingType.MidMotor, 90f, 150f));
        modules.Add(new EngineModule(Fuselage.Tiny, "Basic propeller", 150f, 50f, EngineType.Propeller, 100f, 100f));
        modules.Add(new EngineModule(Fuselage.Tiny, "Fast propeller", 250f, 60f, EngineType.Propeller, 130f, 120f));
        modules.Add(new CockPitModule(Fuselage.Tiny, "Basic cockpit", 100f, 100f, CockpitType.Basic, 3));
        modules.Add(new CockPitModule(Fuselage.Tiny, "Open cockpit", 50f, 40f, CockpitType.Open));
        modules.Add(new LandingGearModule(Fuselage.Tiny, "Fixed landing gear", 50f, 40f, LandingGearType.Fixed));
        modules.Add(new LandingGearModule(Fuselage.Tiny, "Advanced landing gear", 50f, 40f, LandingGearType.Fixed));
        modules.Add(new TailModule(Fuselage.Tiny, "Basic tail", 30f, 30f, TailType.Basic));
        modules.Add(new TailModule(Fuselage.Tiny, "Aerodynamic tail", 50f, 20f, TailType.Aerodynamic));

        modules.Add(new WingModule(Fuselage.Medium, "Basic wing", 100f, 100f, WingType.Low));
        modules.Add(new WingModule(Fuselage.Medium, "Rotor wing", 150f, 120f, WingType.MidMotor, 120f, 150f));
        modules.Add(new EngineModule(Fuselage.Medium, "Basic Motor", 150f, 50f, EngineType.Propeller, 150f, 150f));
        modules.Add(new EngineModule(Fuselage.Medium, "Fast Motor", 150f, 50f, EngineType.FastPropeller, 220f, 180f));
        modules.Add(new CockPitModule(Fuselage.Medium, "Basic Cockpit", 100f, 100f, CockpitType.Basic, 5));
        modules.Add(new CockPitModule(Fuselage.Medium, "Modern Cockpit", 180f, 85f, CockpitType.Advanced, 7));
        modules.Add(new LandingGearModule(Fuselage.Medium, "Basic Landing gear", 50f, 40f, LandingGearType.Fixed));
        modules.Add(new LandingGearModule(Fuselage.Medium, "Retractable Landing gear", 90f, 60f, LandingGearType.Retractable));
        modules.Add(new TailModule(Fuselage.Medium, "Basic Tail", 30f, 30f, TailType.Basic));
        modules.Add(new TailModule(Fuselage.Medium, "Aerodynamic Tail", 60f, 25f, TailType.Aerodynamic));

        return modules;
    }

    public static List<PlaneModule> UnlockableModules()
    {
        List<PlaneModule> all = ModuleCreator.AllModules();
        List<PlaneModule> starter = ModuleCreator.StarterModules();

        //List<PlaneModule> modules = new List<PlaneModule>();
        //modules = starter.Except(all).ToList();
        all.RemoveAll(item => starter.Contains(item));

        return all;
    }

}
