﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

// Plane design
// Avilable fuselages?
// Avilable modules?

public class PlaneDesigner : MonoBehaviour {

    public const int PRICE_EXPENSIVE =          750; // Above
    public const int PRICE_CHEAP =              550; // Below
    public const int RANGE_LONG =               400; // Above
    public const int RANGE_SHORT =              200; // Below
    public const float WEIGHTSPEEDRATIO_FAST =  1.7f; // Below
    public const float WEIGHTSPEEDRATIO_SLOW =  2.7f; // Above

    //public Fuselage UnlockedFuselage = Fuselage.Tiny; // Highest unlocked fuselage
    public List<PlaneModule> AllModules = new List<PlaneModule>(); // All modules
    public List<PlaneModule> UnlockedModules = new List<PlaneModule>();
    public List<PlaneModule> TinyModules = new List<PlaneModule>();
    public List<PlaneModule> MediumModules = new List<PlaneModule>();
    private List<PlaneModule> currentModules = new List<PlaneModule>();

    public Dictionary<string, GameObject> ModuleDictionary = new Dictionary<string, GameObject>();

    public GameObject SlotSelectionContainer;
    public GameObject DesignerContainer;
    public GameObject FuselageSelectionContainer;

    public InputField PlaneNameInputField;
    public GameObject FuselageTinyContainer;
    public GameObject FuselageMediumContainer;

    public GameObject PartGroupButtons;
    public GameObject PartListButtons;

    // Slot buttons
    [Header("Slot buttons")]
    public Button Slot1Button;
    public Button Slot2Button;
    public Button Slot3Button;
    public Button Slot1NewDesignButton;
    public Button Slot2NewDesignButton;
    public Button Slot3NewDesignButton;
    private List<Text> slotButtonTexts;
    private List<Text> slotFuselageTexts;

    [Space(5)]
    public RawImage[] ScreenShots;

    // Fuselage size buttons
    [Header("Fuselage buttons")]
    public Button SmallButton;
    public Button MediumButton;
    public Button HugeButton;

    // Part group selection
    [Header("Part category buttons")]
    public Button CockpitButton;
    public Button EngineButton;
    public Button WingButton;
    public Button LandingGearButton;
    public Button TailButton;
    public Button TestFlightButton; // Done, save design

    [Space(5)]
    public Text PlaneCostText;
    public GameObject fuselageTinySupport;
    public GameObject fuselageMediumSupport;

    // Part list
    [Header("Part buttons")]
    public List<Button> PartButtons;

    [Header("Design feedback text fields")]
    public Text PartListText;
    public Text PartTraitText;

    [Space(5)]
    public Text tabViewText;
    public RectTransform ScreenshotBorder;

    public PlaneDesign[] designSlots;
    private PlaneDesign selectedDesign;
    private int selectedSlot = 0;

    // List for all possible parts, not just unlocked ones?
    private Dictionary<string, GameObject> tinyFuselagePartsAll;
    private Dictionary<string, GameObject> mediumFuselagePartsAll;
    private Dictionary<string, GameObject> tinyFuselagePartsUnlocked;
    private Dictionary<string, GameObject> mediumFuselagePartsUnlocked;
    private Dictionary<string, GameObject> currentFuselageParts;

    private GameObject activeEngine;
    private GameObject activeWingR;
    private GameObject activeWingL;
    private GameObject activeLandingGear;
    private GameObject activeCockpit;
    private GameObject activeTail;

    public PlaneDesign noDesign; // For "empty" slots


    // Use this for initialization
    void Start ()
    {
        //UnlockedModules = new List<PlaneModule>();
        designSlots = new PlaneDesign[3];
        slotButtonTexts = new List<Text>();
        slotFuselageTexts = new List<Text>();
        slotButtonTexts.Add(Slot1Button.GetComponentsInChildren<Text>()[0]);
        slotButtonTexts.Add(Slot2Button.GetComponentsInChildren<Text>()[0]);
        slotButtonTexts.Add(Slot3Button.GetComponentsInChildren<Text>()[0]);
        slotFuselageTexts.Add(Slot1Button.GetComponentsInChildren<Text>()[1]);
        slotFuselageTexts.Add(Slot2Button.GetComponentsInChildren<Text>()[1]);
        slotFuselageTexts.Add(Slot3Button.GetComponentsInChildren<Text>()[1]);

        noDesign.FuselageType = Fuselage.None;
        for (int i = 0; i < 3; i++)
        {
            designSlots[i] = noDesign;
            slotButtonTexts[i].text = "Empty slot";
            slotFuselageTexts[i].text = "- None";
        }

        PlaneNameInputField.onEndEdit.AddListener(delegate { lockInput(PlaneNameInputField); });
        // TEMP BUTTONS
        Slot1Button.GetComponent<Button>().onClick.AddListener(() => { selectedSlot = 0; SelectSlotDesign(); });
        Slot2Button.GetComponent<Button>().onClick.AddListener(() => { selectedSlot = 1; SelectSlotDesign(); });
        Slot3Button.GetComponent<Button>().onClick.AddListener(() => { selectedSlot = 2; SelectSlotDesign(); });
        Slot1NewDesignButton.onClick.AddListener(() => { Debug.Log("New design on slot 1"); selectedSlot = 0; SelectFuselageSize(); });
        Slot2NewDesignButton.onClick.AddListener(() => { Debug.Log("New design on slot 2"); selectedSlot = 1; SelectFuselageSize(); });
        Slot3NewDesignButton.onClick.AddListener(() => { Debug.Log("New design on slot 3"); selectedSlot = 2; SelectFuselageSize(); });

        SmallButton.onClick.AddListener(() => StartNewDesign(Fuselage.Tiny)); // TEMP!
        MediumButton.onClick.AddListener(() => StartNewDesign(Fuselage.Medium));
        HugeButton.onClick.AddListener(() => StartNewDesign(Fuselage.Huge));

        CockpitButton.GetComponent<Button>().onClick.AddListener(() => selectGroupType(PartType.Cockpit));
        EngineButton.GetComponent<Button>().onClick.AddListener(() => selectGroupType(PartType.Engine));
        WingButton.GetComponent<Button>().onClick.AddListener(() => selectGroupType(PartType.Wing));
        LandingGearButton.GetComponent<Button>().onClick.AddListener(() => selectGroupType(PartType.LandingGear));
        TailButton.GetComponent<Button>().onClick.AddListener(() => selectGroupType(PartType.Tail));
        TestFlightButton.onClick.AddListener(() => SaveDesign());

        //tinyFuselageParts = new Dictionary<string, GameObject>();
        //foreach (PlaneModule part in UnlockedModules)
        //{
        //    if (part.partType == PartType.Wing)
        //    {
        //        tinyFuselageParts[part.name + "Right"] = GameObject.Find(part.name + "Right");
        //        tinyFuselageParts[part.name + "Left"] = GameObject.Find(part.name + "Left");
        //        tinyFuselageParts[part.name + "Right"].SetActive(false);
        //        tinyFuselageParts[part.name + "Left"].SetActive(false);
        //    }
        //    else
        //    {
        //        tinyFuselageParts[part.name] = GameObject.Find(part.name);
        //        if (tinyFuselageParts[part.name] != null)
        //        {
        //            tinyFuselageParts[part.name].SetActive(false);
        //            //Debug.Log(part.name + " is NULL");
        //        }
        //    }
        //}
        //Debug.Log("Tiny fuselage parts found: " + tinyFuselageParts.Count);
        
        SlotSelectionContainer.SetActive(true);
        DesignerContainer.SetActive(false);
        PartGroupButtons.SetActive(false);
        PartListButtons.SetActive(false);
	}

    public void CreatePartDictionary()
    {
        tinyFuselagePartsAll = new Dictionary<string, GameObject>();
        mediumFuselagePartsAll = new Dictionary<string, GameObject>();

        // check what fuselage here
        foreach (PlaneModule part in AllModules)
        {
            if (part.supportedFuselage == Fuselage.Tiny)
            {
                if (part.partType == PartType.Wing)
                {
                    tinyFuselagePartsAll[part.name + "Right"] = GameObject.Find(part.name + "Right");
                    tinyFuselagePartsAll[part.name + "Left"] = GameObject.Find(part.name + "Left");
                    tinyFuselagePartsAll[part.name + "Right"].SetActive(false);
                    tinyFuselagePartsAll[part.name + "Left"].SetActive(false);
                }
                else
                {
                    tinyFuselagePartsAll[part.name] = GameObject.Find(part.name);
                    if (tinyFuselagePartsAll[part.name] != null)
                    {
                        tinyFuselagePartsAll[part.name].SetActive(false);
                        //Debug.Log(part.name + " is NULL");
                    }
                }
            }
            else if (part.supportedFuselage == Fuselage.Medium)
            {
                if (part.partType == PartType.Wing)
                {
                    mediumFuselagePartsAll[part.name + "Right"] = GameObject.Find(part.name + "Right");
                    mediumFuselagePartsAll[part.name + "Left"] = GameObject.Find(part.name + "Left");
                    mediumFuselagePartsAll[part.name + "Right"].SetActive(false);
                    mediumFuselagePartsAll[part.name + "Left"].SetActive(false);
                }
                else
                {
                    mediumFuselagePartsAll[part.name] = GameObject.Find(part.name);
                    if (mediumFuselagePartsAll[part.name] != null)
                    {
                        mediumFuselagePartsAll[part.name].SetActive(false);
                        //Debug.Log(part.name + " is NULL");
                    }
                }
            }
        }
        // TODO Add unlock mechanic
        //tinyFuselagePartsUnlocked = tinyFuselagePartsAll;
        //mediumFuselagePartsUnlocked = mediumFuselagePartsAll;
        Debug.Log("Tiny fuselage parts found: " + tinyFuselagePartsAll.Count);
        Debug.Log("Medium fuselage parts found: " + mediumFuselagePartsAll.Count);
    }

    public void AddModuleToUnlocked(PlaneModule module)
    {
        if (UnlockedModules.Contains(module))
        {
            Debug.Log("Already unlocked: " + module.name);
            return;
        }
        UnlockedModules.Add(module); // Check for duplicates?
        switch(module.supportedFuselage)
        {
            case Fuselage.Tiny:
            case Fuselage.Small:
                TinyModules.Add(module);
                break;
            case Fuselage.Medium:
                MediumModules.Add(module);
                break;
            default:
                Debug.Log("Unsupported fuselage: " + module.supportedFuselage);
                break;
        }
    }

    public void AddModulesToUnlocked(List<PlaneModule> modules)
    {
        //UnlockedModules.AddRange(modules);
        foreach (PlaneModule pm in modules)
        {
            AddModuleToUnlocked(pm);
        }
        //foreach (PlaneModule pm in UnlockedModules)
        //{
        //    if (pm.supportedFuselage == Fuselage.Medium && pm.name.IndexOf("Cockpit") > -1)
        //    {
        //        Debug.Log("UNLOCKED: " + pm.name);
        //    }
        //}
    }

    public void AddModuleToDesigner(PlaneModule module)
    {
        AllModules.Add(module);
        //switch (module.supportedFuselage)
        //{
        //    case Fuselage.Tiny:
        //    case Fuselage.Small:
        //        TinyModules.Add(module);
        //        break;
        //    case Fuselage.Medium:
        //        MediumModules.Add(module);
        //        break;
        //    default:
        //        Debug.Log("Unsupported fuselage: " + module.supportedFuselage);
        //        break;
        //}
    }

    public void AddModulesToDesigner(List<PlaneModule> modules)
    {
        foreach (PlaneModule pm in modules)
        {
            AddModuleToDesigner(pm);
            // TEMP - TODO Add unlock mechanic
            //AddModuleToUnlocked(pm);
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    public void SelectSlotDesign()
    {
        if (designSlots[selectedSlot].FuselageType == Fuselage.None)
        {
            // Start new
            SelectFuselageSize();
        }
        else
        {
            // Load existing
            Debug.Log("Load design: " + designSlots[selectedSlot].Name);
            LoadDesign(designSlots[selectedSlot]);
        }
    }

    public void SelectFuselageSize()
    {
        SlotSelectionContainer.SetActive(false);
        FuselageSelectionContainer.SetActive(true);
        tabViewText.text = "SELECT FUSELAGE";
    }

    // Start new design from scratch
    public void StartNewDesign(Fuselage fuselage)
    {
        // Activate design gameobject in ui tree?
        selectedDesign = new PlaneDesign();
        selectedDesign.FuselageType = fuselage;
        selectedDesign.Name = "New Design";

        switch(fuselage)
        {
            case Fuselage.Tiny:
            case Fuselage.Small:
                foreach (KeyValuePair<string, GameObject> kvp in tinyFuselagePartsAll)
                {
                    if (kvp.Value == null)
                    {
                        Debug.Log(kvp.Key);
                    }
                    kvp.Value.SetActive(false);
                }
                break;
            case Fuselage.Medium:
                foreach (KeyValuePair<string, GameObject> kvp in mediumFuselagePartsAll)
                {
                    if (kvp.Value == null)
                    {
                        Debug.Log(kvp.Key);
                    }
                    kvp.Value.SetActive(false);
                }
                break;
        }

        PlaneCostText.text = calculateDesignPrice(selectedDesign).ToString("C0");
        PartTraitText.text = "";
        PartListText.text = "";
        OpenDesigner();
        Debug.Log(fuselage);
        tabViewText.text = "CUSTOMIZE";
    }

    // Load an existing design
    public void LoadDesign(PlaneDesign design)
    {
        selectedDesign = design;
        OpenDesignerWithDesign();
        if (design.LandingGear != null)
            activatePart(design.LandingGear);
        if (design.Cockpit != null)
            activatePart(design.Cockpit);
        if (design.Tail != null)
            activatePart(design.Tail);
        if (design.Wings != null)
            activatePart(design.Wings);
        if (design.Engines != null)
            activatePart(design.Engines);
    }

    public void SaveDesign()
    {
        // Check if full design?
        selectedDesign.Weight = calculateDesignWeight(selectedDesign);
        selectedDesign.Speed = calculateDesignSpeed(selectedDesign);
        selectedDesign.Range = calculateDesignRange(selectedDesign);
        selectedDesign.ManufactureCost = calculateDesignPrice(selectedDesign);
        selectedDesign.PriceAsk = selectedDesign.ManufactureCost;
        selectedDesign.PassengerCount = calculateDesignPassengers(selectedDesign);

        designSlots[selectedSlot] = selectedDesign;
        
        slotButtonTexts[selectedSlot].text = selectedDesign.Name;
        slotFuselageTexts[selectedSlot].text = "- " + selectedDesign.FuselageType;
        StartCoroutine(SaveDesignGraphicCORO());
    }
    
    public IEnumerator SaveDesignGraphicCORO()
    {
        // Wait for end of frame, then save design graphic and set it to button
        var worldCorners = new Vector3[4];
        ScreenshotBorder.GetWorldCorners(worldCorners);
        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        int width = (int)(worldCorners[2].x - worldCorners[0].x);
        int height = (int)(worldCorners[2].y - worldCorners[0].y);
        //Debug.Log("Width: " + width + ". Height: " + height);
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        // Read screen contents into the texture
        tex.ReadPixels(new Rect(worldCorners[0].x, worldCorners[0].y, worldCorners[0].x + width, worldCorners[0].y + height), 0, 0);
        tex.Apply();
        ScreenShots[selectedSlot].texture = tex;
        ScreenShots[selectedSlot].enabled = true;

        //byte[] bytes = tex.EncodeToPNG();
        //Destroy(tex);

        // For testing purposes, also write to a file in the project folder
        //File.WriteAllBytes(Application.dataPath + "/../SavedScreen.png", bytes);
    }


    public void AddDesignSlot()
    {
        // TODO
    }

    public void OpenDesigner()
    {
        // Do other things?
        SlotSelectionContainer.SetActive(false);
        // Check fuselage size and activate correct container
        openFuselageContainer(selectedDesign.FuselageType);
        PlaneNameInputField.text = "";
        DesignerContainer.SetActive(true);
        PartGroupButtons.SetActive(true);
        PartListButtons.SetActive(true);
        selectGroupType(PartType.Cockpit);
    }
    public void OpenDesignerWithDesign()
    {
        SlotSelectionContainer.SetActive(false);
        openFuselageContainer(selectedDesign.FuselageType);
        PlaneNameInputField.text = selectedDesign.Name;
        PlaneCostText.text = calculateDesignPrice(selectedDesign).ToString("C0");
        DesignerContainer.SetActive(true);
        PartGroupButtons.SetActive(true);
        PartListButtons.SetActive(true);
        selectGroupType(PartType.Cockpit);
    }

    private void lockInput(InputField input)
    {
        if (input.text.Length > 0)
        {
            //Debug.Log("Text has been entered");
            selectedDesign.Name = input.text;
            Debug.Log(selectedDesign.Name);
        }
        else if (input.text.Length == 0)
        {
            //Debug.Log("Main Input Empty");
        }
    }

    private void openFuselageContainer(Fuselage container)
    {
        // First close all containers
        FuselageTinyContainer.SetActive(false);
        FuselageMediumContainer.SetActive(false);

        // Open selected fuselage
        switch (container)
        {
            case Fuselage.Tiny:
            case Fuselage.Small:
            default:
                currentModules = TinyModules;
                currentFuselageParts = tinyFuselagePartsAll;
                //Debug.Log("Tiny modules: " + currentModules.Count);
                //Debug.Log("Tiny kvp: " + currentFuselageParts.Count);
                FuselageTinyContainer.SetActive(true);
                break;
            case Fuselage.Medium:
                currentModules = MediumModules;
                currentFuselageParts = mediumFuselagePartsAll;
                FuselageMediumContainer.SetActive(true);
                break;
        }
    }

    private void selectGroupType(PartType type)
    {
        int i = 0;
        foreach ( PlaneModule module in currentModules)
        {
            if (module.partType == type)
            {
                PartButtons[i].gameObject.SetActive(true);
                PartButtons[i].GetComponentInChildren<Text>().text = module.name;
                PartButtons[i].onClick.RemoveAllListeners();
                PartButtons[i].onClick.AddListener(() => activatePart(module));
                i++;
            }
        }
        while (i < PartButtons.Count)
        {
            PartButtons[i].gameObject.SetActive(false);
            i++;
        }
    }

    private void activatePart(PlaneModule part)
    {
        switch(part.partType)
        {
            case PartType.Engine:
                if (activeEngine != null)
                {
                    activeEngine.SetActive(false);
                }
                selectedDesign.Engines = (EngineModule)part;
                break;
            case PartType.Wing:
                if (activeWingR != null)
                {
                    activeWingR.SetActive(false);
                }
                if (activeWingL != null)
                {
                    activeWingL.SetActive(false);
                }
                selectedDesign.Wings = (WingModule)part;
                break;
            case PartType.LandingGear:
                if (activeLandingGear != null)
                {
                    activeLandingGear.SetActive(false);
                }

                selectedDesign.LandingGear = (LandingGearModule)part;
                fuselageTinySupport.SetActive(false);
                fuselageMediumSupport.SetActive(false);
                break;
            case PartType.Cockpit:
                if (activeCockpit != null)
                {
                    activeCockpit.SetActive(false);
                }
                selectedDesign.Cockpit = (CockPitModule)part;
                break;
            case PartType.Tail:
                if (activeTail != null)
                {
                    activeTail.SetActive(false);
                }
                selectedDesign.Tail = (TailModule)part;
                break;
        }
        
        // Activation
        if (part.partType == PartType.Engine)
        {
            currentFuselageParts[part.name].SetActive(true);
            activeEngine = currentFuselageParts[part.name];
        }
        else if (part.partType == PartType.Wing)
        {
            currentFuselageParts[part.name + "Right"].SetActive(true);
            currentFuselageParts[part.name + "Left"].SetActive(true);
            activeWingR = currentFuselageParts[part.name + "Right"];
            activeWingL = currentFuselageParts[part.name + "Left"];
            
        }
        else if (part.partType == PartType.Cockpit)
        {
            currentFuselageParts[part.name].SetActive(true);
            activeCockpit = currentFuselageParts[part.name];
        }
        else if (part.partType == PartType.LandingGear)
        {
            currentFuselageParts[part.name].SetActive(true);
            activeLandingGear = currentFuselageParts[part.name];
        }
        else if (part.partType == PartType.Tail)
        {
            currentFuselageParts[part.name].SetActive(true);
            activeTail = currentFuselageParts[part.name];
        }
        // Update the text on screen
        updateDesignListing();
        updateDesignTraits();
        PlaneCostText.text = calculateDesignPrice(selectedDesign).ToString("C0");
    }

    // Update the text listing of parts
    private void updateDesignListing()
    {
        string designString = "";
        designString += "\n" + selectedDesign.FuselageType + " Fuselage";
        if (selectedDesign.Wings != null)
        {
            //designString += "\n" + selectedDesign.Wings.name;
            designString = designString + "\n" + selectedDesign.Wings.name;
        }
        if (selectedDesign.Engines != null)
            designString = designString + "\n" + selectedDesign.Engines.name;
        if (selectedDesign.Cockpit != null)
            designString = designString + "\n" + selectedDesign.Cockpit.name;
        if (selectedDesign.LandingGear != null)
            designString = designString + "\n" + selectedDesign.LandingGear.name;
        if (selectedDesign.Tail != null)
            designString = designString + "\n" + selectedDesign.Tail.name;

        // Debug.Log does not like excessive "\n"s 
        //Debug.Log("Design Debug: " + designString);
        //Debug.Log("Weight: " + calculateDesignWeight(selectedDesign) + 
        //    ",  Speed: " + calculateDesignSpeed(selectedDesign) + 
        //    ",  Price: " + calculateDesignPrice(selectedDesign) +
        //    ",  Range: " + calculateDesignRange(selectedDesign));
        PartListText.text = designString;
    }

    // Update the trait table (fast, cheap, etc)
    private void updateDesignTraits()
    {
        string partTraitText = "";
        float totalWeight = calculateDesignWeight(selectedDesign);
        float totalSpeed = calculateDesignSpeed(selectedDesign);
        float totalPrice = calculateDesignPrice(selectedDesign);
        float totalRange = calculateDesignRange(selectedDesign);
        // Weight / Speed ratio for speed stat
        // Pure range for range stat
        // Pure price for price stat
        if (totalSpeed != 0)
        {
            if (totalWeight / totalSpeed > WEIGHTSPEEDRATIO_SLOW)
            {
                // Slow
                partTraitText += "Slow";
            }
            else if (totalWeight / totalSpeed < WEIGHTSPEEDRATIO_FAST)
            {
                // Fast
                partTraitText += "Fast";
            }
            else
            {
                // Normal
            }
        }
        if (totalRange >= RANGE_LONG)
        {
            // Long range
            partTraitText += "\nLong range";
        }
        else if (totalRange <= RANGE_SHORT)
        {
            // Low range
            partTraitText += "\nShort range";
        }
        else
        {
            // Normal range
        }
        if (totalPrice >= PRICE_EXPENSIVE)
        {
            // Expensive
            partTraitText += "\nExpensive";
        }
        else if (totalPrice <= PRICE_CHEAP)
        {
            // Cheap
            partTraitText += "\nCheap";
        }
        else
        {
            // Normal price
        }
        PartTraitText.text = partTraitText;
    }

    private float calculateDesignRange(PlaneDesign design)
    {
        float range = 0;
        range += (int)design.FuselageType + 1 * 100;
        if (design.Wings != null)
            range += design.Wings.range;
        if (design.Engines != null)
            range += design.Engines.range;

        return range;
    }

    private float calculateDesignPrice(PlaneDesign design)
    {
        float price = 0;
        price += (int)(design.FuselageType+1) * 100;
        if (design.Wings != null)
            price += design.Wings.basePrice;
        if (design.Cockpit != null)
            price += design.Cockpit.basePrice;
        if (design.Engines != null)
            price += design.Engines.basePrice;
        if (design.LandingGear != null)
            price += design.LandingGear.basePrice;
        if (design.Tail != null)
            price += design.Tail.basePrice;

        return price;
    }

    private float calculateDesignWeight(PlaneDesign design)
    {
        float weight = 0;
        weight += (int)design.FuselageType * 50;
        if (design.Wings != null)
            weight += design.Wings.weight;
        if (design.Cockpit != null)
            weight += design.Cockpit.weight;
        if (design.Engines != null)
            weight += design.Engines.weight;
        if (design.LandingGear != null)
            weight += design.LandingGear.weight;
        if (design.Tail != null)
            weight += design.Tail.weight;

        return weight;
    }

    private float calculateDesignSpeed(PlaneDesign design)
    {
        float speed = 0;
        if (design.Engines != null)
            speed += design.Engines.speed;
        if (design.Wings != null)
        {
            if (design.Wings.wingtype > WingType.High) // Motor wing
            {
                if (speed == 0) // No other engine
                {
                    speed += design.Wings.speed;
                }
                else // Main engine present, only add half speed
                {
                    speed += design.Wings.speed/2f;
                }
            }
        }
        return speed;
    }

    private int calculateDesignPassengers(PlaneDesign design)
    {
        int passengerCount = 0;
        if (design.Cockpit != null)
            passengerCount += design.Cockpit.passengers;
        if (design.Cabin != null)
            passengerCount += design.Cabin.passengers;
        return passengerCount;
    }

    private void disableActivePartObjects()
    {
        if (activeEngine)
            activeEngine.SetActive(false);
        if (activeCockpit)
            activeCockpit.SetActive(false);
        if (activeWingL)
            activeWingL.SetActive(false);
        if (activeWingR)
            activeWingR.SetActive(false);
        if (activeLandingGear)
            activeLandingGear.SetActive(false);
        if (activeTail)
            activeTail.SetActive(false);
    }

    void OnDisable()
    {
        //Debug.Log("PlaneDesigner disabled");
        SlotSelectionContainer.SetActive(true);
        fuselageTinySupport.SetActive(true);
        fuselageMediumSupport.SetActive(true);
        disableActivePartObjects();
        DesignerContainer.SetActive(false);
        PartGroupButtons.SetActive(false);
        PartListButtons.SetActive(false);
        FuselageSelectionContainer.SetActive(false);
    }
}
