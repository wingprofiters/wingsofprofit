﻿using UnityEngine;
using UnityEngine.UI;

// Class to manage admin view tab functionality
// Month end report, sales, profits, losses?
public class AdministrationView : MonoBehaviour {

    public bool DesignTabVisited = false;
    public bool MarketingTabVisited = false;
    public bool ResearchTabVisited = false;

    public Text totalProfit;
    public Text income;
    public Text loanPayback;
    public Text Month;
    public Text financingText;
    public Text totalExpenses;
    public Text researchExpenceText;
    public Text marketingExpenceText;
    

    public int loanPayBackMonthly =0;
    public int financing = 0;
    public int totalProfitMonthly =0;
    public int profitFromSales = 0;
    public int researchExpence = 0;
    public int marketingExpence = 0;

    public Button EndMonthButton;

    public GameObject ConfirmationPromptContainer;
    public Button ConfirmationYes;
    public Button ConfirmationNo;

    private GameManager gameManager;

    public Sprite payLoanImage;
    public Sprite takeLoanImage;

    public Sprite loanOnImage;
    public Sprite loanOffImage;

    public int loan1Remaining;
    public int loan2Remaining;
    public int loan3Remaining;

    public Button loan1Button;
    public Button loan2Button;
    public Button loan3Button;

    public Image loan1State;
    public Image loan2State;
    public Image loan3State;

    public Text loan1Text;
    public Text loan2Text;
    public Text loan3Text;

    // Use this for initialization
    void Start () {
        gameManager = FindObjectOfType<GameManager>();
        EndMonthButton.onClick.AddListener(() => EndMonth());
        ConfirmationYes.onClick.AddListener(() => confirmChoice(true));
        ConfirmationNo.onClick.AddListener(() => confirmChoice(false));
        UpdateTextFields();      
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateTextFields()
    {
        if (loan1Remaining > 0)
        {
            loan1Text.text = "10 000$ Loan taken / " + loan1Remaining + " still to pay";
        }else
        {
            loan1Text.text = "10 000$ payback: 500$ + 100$ financing";
        }
        if (loan2Remaining > 0) 
        {
            loan2Text.text = "50 000$ Loan taken / " + loan2Remaining + " still to pay";
        }
        else
        {
            loan2Text.text = "50 000$ payback: 1500$ + 300$ financing";
        }
        if (loan3Remaining > 0)
        {
            loan3Text.text = "100 000$ Loan taken / " + loan3Remaining + " still to pay";
        }
        else
        {
            loan3Text.text = "100 000$ payback: 4500$ + 800$ financing";
        }
    }

    public void LoanButtonOpertator(int loanlevel)
    {
        if (loanlevel ==1 && loan1Remaining <= 0)
        {
            GetLoan(1);
        }
        else if(loanlevel == 1 && loan1Remaining > 0)
        {
            PayLoan(1);
        }
        else if(loanlevel == 2 && loan2Remaining <= 0)
        {
            GetLoan(2);
        }
        else if (loanlevel == 2 && loan2Remaining > 0)
        {
            PayLoan(2);
        }
        else if (loanlevel == 3 && loan3Remaining <= 0)
        {
            GetLoan(3);
        }
        else if (loanlevel == 3 && loan3Remaining > 0)
        {
            PayLoan(3);
        }
    }
    public void PayLoan (int loanLevel) {
        if (loanLevel == 1 && gameManager.CompanyFunds > loan1Remaining)
        {
            gameManager.UpdateFunds(-loan1Remaining);
            loanPayBackMonthly += loan1Remaining;
            loan1Remaining = 0;
            loan1State.sprite = loanOffImage;
            loan1Button.image.sprite = takeLoanImage;
        }
        else if(loanLevel == 2 && gameManager.CompanyFunds > loan2Remaining)
        {
            gameManager.UpdateFunds(-loan2Remaining);
            loanPayBackMonthly += loan2Remaining;
            loan2Remaining = 0;
            loan2State.sprite = loanOffImage;
            loan2Button.image.sprite = takeLoanImage;
        }
        else if (loanLevel == 3 && gameManager.CompanyFunds > loan3Remaining)
        {
            gameManager.UpdateFunds(-loan3Remaining);
            loanPayBackMonthly += loan3Remaining;
            loan3Remaining = 0;
            loan3State.sprite = loanOffImage;
            loan3Button.image.sprite = takeLoanImage;
        }
        UpdateTextFields();
    }
    public void GetLoan(int loanLevel)
    {
        int loan =0;
        if (loanLevel == 1)
        {
            loan = 10000;
            loan1State.sprite = loanOnImage;
            loan1Button.image.sprite = payLoanImage;
            loan1Remaining = loan;
        }
        else if(loanLevel == 2)
        {
            loan = 50000;
            loan2State.sprite = loanOnImage;
            loan2Button.image.sprite = payLoanImage;
            loan2Remaining = loan;
        }
        else if (loanLevel == 3)
        {
            loan = 100000;
            loan3State.sprite = loanOnImage;
            loan3Button.image.sprite = payLoanImage;
            loan3Remaining = loan;
        }
        gameManager.UpdateFunds(loan);
        UpdateTextFields();
    }
    public void PayLoansMonth()
    {       
        if (loan1Remaining > 0)
        {
            financing += 100;
            if (loan1Remaining < 500)
            {
                gameManager.UpdateFunds(-loan1Remaining);
                loanPayBackMonthly += loan1Remaining;
                loan1Remaining = 0;
            
            }
            else
            {
                loan1Remaining -= 500;
                loanPayBackMonthly += 500;
                gameManager.UpdateFunds(-500);
            }
        }
        if (loan2Remaining > 0)
        {
            financing += 300;
            if (loan2Remaining < 1500)
            {
                gameManager.UpdateFunds(-loan2Remaining);
                loanPayBackMonthly += loan2Remaining;
                loan2Remaining = 0;
            }
            else
            {
                loan2Remaining -= 1500;
                gameManager.UpdateFunds(-1500);
                loanPayBackMonthly += 1500;
            }
        }
        if (loan3Remaining > 0)
        {
            financing += 800;
            if (loan3Remaining < 4500)
            {
                gameManager.UpdateFunds(-loan3Remaining);
                loanPayBackMonthly += loan3Remaining;
                loan3Remaining = 0;
            }
            else
            {
                loan3Remaining -= 4500;
                gameManager.UpdateFunds(-4500);
                loanPayBackMonthly += 4500;
            }
        }
        gameManager.UpdateFunds(-financing);
        UpdateTextFields();
    }
    public void EndMonth()
    {
        // Check if all tabs are visited and warn if not
        if (DesignTabVisited && MarketingTabVisited && ResearchTabVisited)
        {
            // End month
            Debug.Log("End month, no prompt");
            DesignTabVisited = false;
            MarketingTabVisited = false;
            ResearchTabVisited = false;
            PayLoansMonth();
            gameManager.AdvanceTime();
            Month.text = gameManager.monthNames[(gameManager.GameMonth - 1) % 12] + " " + (1960 + gameManager.GameMonth / 12);
            researchExpence = (int)gameManager.researchBudget.researchBudget;
            marketingExpence = gameManager.marketView.calculateTotalMarketingBudget();
            profitFromSales = gameManager.IncomeLastMonth;
            income.text = profitFromSales.ToString();
            financingText.text = (-financing).ToString();
            totalExpenses.text = (-financing - loanPayBackMonthly - researchExpence - marketingExpence).ToString();
            profitFromSales -= (financing + loanPayBackMonthly + researchExpence + marketingExpence);
            loanPayback.text = (-loanPayBackMonthly).ToString();
            researchExpenceText.text = (-researchExpence).ToString();
            marketingExpenceText.text = (-marketingExpence).ToString();
            financing = 0;
            loanPayBackMonthly = 0;
            totalProfit.text = profitFromSales.ToString();
            profitFromSales = 0;

        }
        else
        {
            // Ask if sure
            Debug.Log("End month, show prompt");
            ConfirmationPromptContainer.SetActive(true);
           
        }
    }

    private void confirmChoice(bool choice)
    {
        if(choice)
        {
            Debug.Log("Confirmation yes, end month");
            DesignTabVisited = false;
            MarketingTabVisited = false;
            ResearchTabVisited = false;
            PayLoansMonth();
            gameManager.AdvanceTime();
            Month.text = gameManager.monthNames[(gameManager.GameMonth - 1) % 12] + " " + (1960 + gameManager.GameMonth / 12);
            researchExpence = (int)gameManager.researchBudget.researchBudget;
            marketingExpence = gameManager.marketView.calculateTotalMarketingBudget();
            profitFromSales = gameManager.IncomeLastMonth;
            income.text = profitFromSales.ToString();
            financingText.text = (-financing).ToString();
            totalExpenses.text = (-financing - loanPayBackMonthly-researchExpence-marketingExpence).ToString();
            profitFromSales -= (financing + loanPayBackMonthly + researchExpence + marketingExpence);
            loanPayback.text = (-loanPayBackMonthly).ToString();
            researchExpenceText.text = (-researchExpence).ToString();
            marketingExpenceText.text = (-marketingExpence).ToString();
            financing = 0;
            loanPayBackMonthly = 0;
            totalProfit.text = profitFromSales.ToString();
            profitFromSales = 0;
        }
        else
        {
            Debug.Log("Confirmation no, continue month");
        }
        ConfirmationPromptContainer.SetActive(false);
    }
    
    void OnDisable()
    {
        ConfirmationPromptContainer.SetActive(false);
    }
}
