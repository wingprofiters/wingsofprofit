﻿using System;

// Fuel tank?

# region enums
public enum Fuselage
{
    Tiny,
    Small,
    Medium,
    Large,
    Huge,
    Supersonic,
    SpacePlane,
    None,
}

public enum PartType
{
    Wing,
    Engine,
    Cabin,
    Cockpit,
    LandingGear,
    Tail,
}

public enum WingType
{
    Low,
    Mid,
    High,
    LowMotor,
    MidMotor,
    QuadMotor,
}

public enum EngineType
{
    NoEngine,
    Propeller,
    FastPropeller,
    Jet,
}

public enum CabinType
{
    Cramped,
    Normal,
    Spacious,
}

public enum CockpitType
{
    Basic,
    Advanced,
    Open,
}

public enum LandingGearType
{
    Fixed,
    Retractable,
}

public enum TailType
{
    Basic,
    Aerodynamic,
}
#endregion

// Speed?

// Class that all modules inherit
[Serializable]
public class PlaneModule
{
    public Fuselage supportedFuselage;
    public string name;
    public float basePrice;
    public float weight; // might use
    public PartType partType;

    public new static bool Equals(object x, object y)
    {
        if (x == y) // Reference equality only; overloaded operators are ignored
        {
            return true;
        }
        if (x == null || y == null) // Again, reference checks
        {
            return false;
        }
        return x.Equals(y); // Safe as we know x != null.
    }

    public bool Equals(PlaneModule other)
    {
        //var objectToCompareWith = (PlaneModule)obj;

        return other.supportedFuselage == supportedFuselage && other.name == name;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        var objectToCompareWith = (PlaneModule)obj;

        return objectToCompareWith.supportedFuselage == supportedFuselage && objectToCompareWith.name == name;
    }

    public static bool operator ==(PlaneModule d1, PlaneModule d2)
    {
        return PlaneModule.Equals(d1, d2);
        //return d1.Equals(d2);
    }

    public static bool operator !=(PlaneModule d1, PlaneModule d2)
    {
        return !PlaneModule.Equals(d1, d2);
        //return !d1.Equals(d2);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}

// Wing module
[Serializable]
public class WingModule : PlaneModule
{
    public WingType wingtype;
    public float speed;
    public float range;
    public WingModule(Fuselage fuselage, string name, float basePrice, float weight, WingType wingtype, float speed = 0, float range = 100f)
    {
        this.supportedFuselage = fuselage;
        this.name = name;
        this.basePrice = basePrice;
        this.weight = weight;
        this.wingtype = wingtype;
        this.partType = PartType.Wing;
        this.speed = speed;
        this.range = range;
    }
}

[Serializable]
public class EngineModule : PlaneModule
{
    public EngineType enginetype;
    public float speed;
    public float range;
    public EngineModule(Fuselage fuselage, string name, float basePrice, float weight, EngineType enginetype, float speed, float range)
    {
        this.supportedFuselage = fuselage;
        this.name = name;
        this.basePrice = basePrice;
        this.weight = weight;
        this.enginetype = enginetype;
        this.partType = PartType.Engine;
        this.speed = speed;
        this.range = range;
    }
}

[Serializable]
public class CockPitModule : PlaneModule
{
    public CockpitType cockpittype;
    public int passengers;
    public CockPitModule(Fuselage fuselage, string name, float basePrice, float weight, CockpitType cockpittype, int passengers = 1)
    {
        this.supportedFuselage = fuselage;
        this.name = name;
        this.basePrice = basePrice;
        this.weight = weight;
        this.cockpittype = cockpittype;
        this.partType = PartType.Cockpit;
        this.passengers = passengers;
    }
}

[Serializable]
public class CabinModule : PlaneModule
{
    public CabinType cabintype;
    public int passengers;
    public CabinModule(Fuselage fuselage, string name, float basePrice, float weight, CabinType cabintype)
    {
        this.supportedFuselage = fuselage;
        this.name = name;
        this.basePrice = basePrice;
        this.weight = weight;
        this.cabintype = cabintype;
        this.partType = PartType.Cabin;
    }
}

[Serializable]
public class LandingGearModule : PlaneModule
{
    public LandingGearType geartype;
    public LandingGearModule(Fuselage fuselage, string name, float basePrice, float weight, LandingGearType geartype)
    {
        this.supportedFuselage = fuselage;
        this.name = name;
        this.basePrice = basePrice;
        this.weight = weight;
        this.geartype = geartype;
        this.partType = PartType.LandingGear;
    }
}

[Serializable]
public class TailModule : PlaneModule
{
    public TailType tailtype;
    public TailModule(Fuselage fuselage, string name, float basePrice, float weight, TailType tailtype)
    {
        this.supportedFuselage = fuselage;
        this.name = name;
        this.basePrice = basePrice;
        this.weight = weight;
        this.tailtype = tailtype;
        this.partType = PartType.Tail;
    }
}

// Struct for saving plane design
[Serializable]
public struct PlaneDesign
{
    public Fuselage FuselageType;
    public string Name;
    public WingModule Wings;
    public EngineModule Engines;
    public CockPitModule Cockpit;
    public CabinModule Cabin;
    public LandingGearModule LandingGear;
    public TailModule Tail;
    public float Speed;
    public float Weight;
    public float Range;
    public float ManufactureCost;
    public int PassengerCount;
    public float PriceAsk;
    public bool FinishedDesign; // All parts included

    public bool Equals(PlaneDesign other)
    {
        return Equals(other, this);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        var objectToCompareWith = (PlaneDesign)obj;

        return objectToCompareWith.FuselageType == FuselageType && objectToCompareWith.Name == Name &&
               objectToCompareWith.Wings == Wings && objectToCompareWith.Engines == Engines &&
               objectToCompareWith.Cockpit == Cockpit && objectToCompareWith.Cabin == Cabin &&
               objectToCompareWith.LandingGear == LandingGear && objectToCompareWith.Tail == Tail;
    }

    public static bool operator ==(PlaneDesign d1, PlaneDesign d2)
    {
        return d1.Equals(d2);
    }

    public static bool operator !=(PlaneDesign d1, PlaneDesign d2)
    {
        return !d1.Equals(d2);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
