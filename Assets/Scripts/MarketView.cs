﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketView : MonoBehaviour {

    public GameObject MapContainer;
    public GameObject AreaContainer;
    public Text SummaryInfoText;
    public Text AreaText;

    [Header("Area container")]
    public Button DesignSlot1;
    public Button DesignSlot2;
    public Button DesignSlot3;
    public Text DesignSlotDescription1;
    public Text DesignSlotDescription2;
    public Text DesignSlotDescription3;
    public Text[] DesingSlotFuselageSizeTexts;
    public InputField DesignPrice1;
    public InputField DesignPrice2;
    public InputField DesignPrice3;
    public Button BackButton;
    public Button BackFromSelect;
    public InputField MarketingBudgetInput;
    public Text AreaNameText;
    public Text ProfitLastMonthText;

    public GameObject AddDesignToMarketContainer;
    public List<Button> AddDesignButtons;
    public Button NoDesign; // For removing design from market
    public Text[] ProfitTexts;

    public List<MarketArea> MarketAreas;

    private MarketArea activeArea;
    private GameObject hoverArea;
    private MouseOverScript mouseOverScript;
    private GameManager gameManager;

    private int totalMarketingBudget;

    private int selectedDesignSlot;
    private Button[] areaDesignSlotButtons;
    private InputField[] areaDesignPriceInputs;
    private Text[] areaDesignDescriptions;

    public Image activeAreaImage;
    public Sprite smallEurope;
    public Sprite smallAsia;
    public Sprite smallAfrica;
    public Sprite smallAustralia;
    public Sprite smallSAmerica;
    public Sprite smallNamerica;

    // Use this for initialization
    void Start () {
        //MapContainer.SetActive(true);
        //AreaContainer.SetActive(false);

        areaDesignPriceInputs = new InputField[3];
        areaDesignPriceInputs[0] = DesignPrice1;
        areaDesignPriceInputs[1] = DesignPrice2;
        areaDesignPriceInputs[2] = DesignPrice3;

        areaDesignSlotButtons = new Button[3];
        areaDesignSlotButtons[0] = DesignSlot1;
        areaDesignSlotButtons[1] = DesignSlot2;
        areaDesignSlotButtons[2] = DesignSlot3;

        areaDesignDescriptions = new Text[3];
        areaDesignDescriptions[0] = DesignSlotDescription1;
        areaDesignDescriptions[1] = DesignSlotDescription2;
        areaDesignDescriptions[2] = DesignSlotDescription3;

        gameManager = FindObjectOfType<GameManager>();
        mouseOverScript = FindObjectOfType<MouseOverScript>();
        MarketAreas = new List<MarketArea>();
        foreach (MarketArea ma in FindObjectsOfType<MarketArea>())
        {
            MarketAreas.Add(ma);
        }
        BackButton.onClick.AddListener(() => { activeArea = null; AreaContainer.SetActive(false); mouseOverScript.MouseOverEnabled = true; });
        BackFromSelect.onClick.AddListener(() => { AddDesignToMarketContainer.SetActive(false); });
        NoDesign.onClick.AddListener(() => removeDesignFromSelectedSlot());
        areaDesignSlotButtons[0].onClick.AddListener(() => SelectDesignSlot(0));
        areaDesignSlotButtons[1].onClick.AddListener(() => SelectDesignSlot(1));
        areaDesignSlotButtons[2].onClick.AddListener(() => SelectDesignSlot(2));

        // Input fields
        MarketingBudgetInput.onEndEdit.AddListener(delegate { changeMarketingBudget(MarketingBudgetInput); });
        DesignPrice1.onEndEdit.AddListener(delegate { changeDesignPriceForSlot(DesignPrice1, 0); });
        DesignPrice2.onEndEdit.AddListener(delegate { changeDesignPriceForSlot(DesignPrice2, 1); });
        DesignPrice3.onEndEdit.AddListener(delegate { changeDesignPriceForSlot(DesignPrice3, 2); });

        AddDesignToMarketContainer.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void SelectDesignSlot(int slot) // 0 to 2
    {
        selectedDesignSlot = slot;
        AddDesignToMarketContainer.SetActive(true);
        loadDesignsToAddDesignsWindow();
    }

    void OnDisable()
    {
        //MapContainer.SetActive(true);
        if (mouseOverScript)
            mouseOverScript.MouseOverEnabled = true;
        AreaContainer.SetActive(false);
        activeArea = null;
        AddDesignToMarketContainer.SetActive(false);
    }

    void OnEnable()
    {
        StartCoroutine(displayAreaInfoTextCORO());
        totalMarketingBudget = calculateTotalMarketingBudget();
    }

    private void loadDesignsToAddDesignsWindow()
    {
        foreach(Button b in AddDesignButtons)
        {
            b.onClick.RemoveAllListeners();
            b.GetComponentInChildren<Text>().text = "";
            b.enabled = false;
        }
        foreach(Text t in DesingSlotFuselageSizeTexts)
        {
            t.text = "";
        }
        for (int i = 0; i < 3; i++)
        {
            if (gameManager.GetDesignerScript().designSlots[i].FuselageType != Fuselage.None)
            {
                // Add to button
                AddDesignButtons[i].enabled = true;
                AddDesignButtons[i].GetComponentInChildren<Text>().text = gameManager.GetDesignerScript().designSlots[i].Name;
                DesingSlotFuselageSizeTexts[i].text = "Size: " + gameManager.GetDesignerScript().designSlots[i].FuselageType;
                //int indx = i;
                PlaneDesign pd = gameManager.GetDesignerScript().designSlots[i];
                AddDesignButtons[i].onClick.AddListener(() => addDesignToSlot(pd));
            }
        }
    }

    public int calculateTotalMarketingBudget()
    {
        int budget = 0;
        foreach (MarketArea area in MarketAreas)
        {
            budget += area.MarketingBudget;
        }
        return budget;
    }

    private int calculateTotalMarketingBudgetExcludingArea(MarketArea area)
    {
        int budget = 0;
        foreach (MarketArea ma in MarketAreas)
        {
            if (ma != area)
            {
                budget += area.MarketingBudget;
            }
        }
        return budget;
    }

    private int calculateTotalProfitLastMonth()
    {
        int profit = 0;
        foreach (MarketArea area in MarketAreas)
        {
            profit += area.ProfitLastMonth;
        }
        return profit;
    }

    private void changeMarketingBudget(InputField input)
    {
        if (!String.IsNullOrEmpty(input.text))
        {
            //Debug.Log("Text has been entered");
            int newBudget = -1;
            try
            {
                newBudget = Int32.Parse(input.text);
                if (newBudget > 0 && newBudget + calculateTotalMarketingBudgetExcludingArea(activeArea) <= gameManager.CompanyFunds) // Total budget somewhere?
                {
                    activeArea.MarketingBudget = newBudget;
                    input.text = newBudget.ToString("C0");
                }
                else
                {
                    input.text = activeArea.MarketingBudget.ToString("C0");
                }
            }
            catch
            {
                input.text = activeArea.MarketingBudget.ToString("C0");
            }

        }
        else if (input.text.Length == 0)
        {
            //Debug.Log("Main Input Empty");
            activeArea.MarketingBudget = 0;
            input.text = 0.ToString("C0");
        }
    }

    private void changeDesignPriceForSlot(InputField input, int slot)
    {
        if (!String.IsNullOrEmpty(input.text))
        {
            //Debug.Log("Text has been entered");
            if(activeArea.DesignsForSale[slot].FuselageType != Fuselage.None)
            {
                try
                {
                    int newPrice = Int32.Parse(input.text);
                    if (newPrice > 0)
                    {
                        activeArea.DesignsForSale[slot].PriceAsk = newPrice;
                        input.text = newPrice.ToString("C0");
                        ProfitTexts[slot].text = (newPrice - activeArea.DesignsForSale[slot].ManufactureCost).ToString("C0");
                    }
                    else
                    {
                        input.text = activeArea.DesignsForSale[slot].PriceAsk.ToString("C0");
                    }
                }
                catch
                {
                    input.text = activeArea.DesignsForSale[slot].PriceAsk.ToString("C0");
                }
            }
        }
        else
        {
            input.text = activeArea.DesignsForSale[slot].PriceAsk.ToString("C0");
        }
    }

    private void addDesignToSlot(PlaneDesign design)
    {
        if (design.FuselageType == Fuselage.None) return;
        if (activeArea.AddDesignToMarketSlot(selectedDesignSlot, design))
        {
            areaDesignPriceInputs[selectedDesignSlot].text = design.PriceAsk.ToString("C0");
            areaDesignDescriptions[selectedDesignSlot].text = design.Name; // TODO add more info
            ProfitTexts[selectedDesignSlot].text = (design.PriceAsk - design.ManufactureCost).ToString("C0");
            gameManager.UpdatePlaneMarketAdditionCosts((int)design.ManufactureCost*10);
        }
        AddDesignToMarketContainer.SetActive(false);
    }

    private void removeDesignFromSelectedSlot()
    {
        // remove from selected slot
        activeArea.RemoveDesignFromMarket(selectedDesignSlot);
        areaDesignPriceInputs[selectedDesignSlot].text = "";
        areaDesignDescriptions[selectedDesignSlot].text = "No design";
        ProfitTexts[selectedDesignSlot].text = "";
        AddDesignToMarketContainer.SetActive(false);
    }

    private IEnumerator displayAreaInfoTextCORO()
    {
        SummaryInfoText.text = "";
        yield return new WaitForSeconds(0.01f); // Small wait for marketarea loading on first load
        string s = string.Format("{0, -10} {1,12} {2, 22}\n\n", "Area", "Marketing budget", "Profit last month");

        foreach (MarketArea ma in MarketAreas)
        {
            //SummaryInfoText.text = SummaryInfoText.text + ma.AreaName + ": \t" + ma.MarketingBudget + "\n";
            s += string.Format("{0, -18} {1,8:N0} {2,22}\n", ma.AreaName, ma.MarketingBudget, ma.ProfitLastMonth);
        }
        s += "\n";
        s += string.Format("{0, -18} {1,8:N0} {2,22}\n", "Total", calculateTotalMarketingBudget(), calculateTotalProfitLastMonth());
        SummaryInfoText.text = s;
    }

    /*
    // Adding designs costs something?
    private void addDesigns()
    {
        PlaneDesigner pdes = gameManager.GetDesignerScript();
        for (int i = 0; i < 3; i++)
        {
            if (pdes.designSlots[i].FuselageType != Fuselage.None)
            {
                //activeArea.AddDesignToMarket(pdes.designSlots[i]);
            }
        }
    }
    */
    
    public void ActivateArea(MarketArea area)
    {
        mouseOverScript.MouseOverEnabled = false;
        MouseOverArea(null);
        AreaContainer.SetActive(true);
        if (activeArea == area || area == null) return;
        else
        {
            activeArea = area;
            //AreaText.text = activeArea.AreaName + "\nBudget: " + activeArea.MarketingBudget + "\nDesigns on market: " +
            //    activeArea.DesignsForSale.Count + "\nProfit last month: " + activeArea.ProfitLastMonth +
            //    "\nFame: " + activeArea.Fame;
        }

        MarketingBudgetInput.text = area.MarketingBudget.ToString("C0");
        AreaNameText.text = area.AreaName;        
        ProfitLastMonthText.text = area.ProfitLastMonth.ToString("C0"); ;

        if(area.AreaName == "Europe")
        {
            activeAreaImage.sprite = smallEurope;
        }else if(area.AreaName == "Asia")
        {
            activeAreaImage.sprite = smallAsia;
        }
        else if(area.AreaName == "Africa")
        {
            activeAreaImage.sprite = smallAfrica;
        }
        else if (area.AreaName == "Australia")
        {
            activeAreaImage.sprite = smallAustralia;
        }
        else if(area.AreaName == "North America")
        {
            activeAreaImage.sprite = smallNamerica;
        }
        else if (area.AreaName == "South America")
        {
            activeAreaImage.sprite = smallSAmerica;
        }

        //DesignPrice1.text = "";
        //DesignPrice2.text = "";
        //DesignPrice3.text = "";
        for (int i = 0; i < 3; i++)
        {
            if (activeArea.DesignsForSale[i].FuselageType == Fuselage.None)
            {
                areaDesignPriceInputs[i].text = "";
                areaDesignDescriptions[i].text = "No design";
                ProfitTexts[i].text = "";
            }
            else
            {
                areaDesignPriceInputs[i].text = activeArea.DesignsForSale[i].PriceAsk.ToString("C0");
                areaDesignDescriptions[i].text = activeArea.DesignsForSale[i].Name;
                ProfitTexts[i].text = (activeArea.DesignsForSale[i].PriceAsk - activeArea.DesignsForSale[i].ManufactureCost).ToString("C0");
            }
        }


    }

    public void MouseOverArea(GameObject areaGO)
    {
        if (areaGO == null)
        {
            if (hoverArea) hoverArea.GetComponent<SpriteRenderer>().color = Color.white;
            hoverArea = null;
            return;
        }
        else if (hoverArea) hoverArea.GetComponent<SpriteRenderer>().color = Color.white;
        hoverArea = areaGO;
        areaGO.GetComponent<SpriteRenderer>().color = Color.red;
    }

    public void AreaClicked(GameObject areaGO)
    {
        if (areaGO == null)
        {
            Debug.LogError("ERROR: MarketView.AreaClicked called with null");
        }
        else
        {
            MarketArea ma = areaGO.GetComponent<MarketArea>();
            Debug.Log(ma.AreaName);
            ActivateArea(ma);
            // Do something
        }

    }

    // Random events?
    public int AdvanceTime() // Called at the end of month. Return revenue
    {
        int revenue = 0;
        foreach (MarketArea ma in MarketAreas)
        {
            revenue += ma.AdvanceTime();
            //Debug.Log(ma.AreaName + " TIME+ " + revenue);
        }
        return revenue;
    }
}