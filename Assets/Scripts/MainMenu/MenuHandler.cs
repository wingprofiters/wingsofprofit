﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;


public class MenuHandler : MonoBehaviour {

    private GameObject firstFolder;
    private GameObject firstGame;
    private GameObject loadGame;
    private GameObject optionsFolder;
    private GameObject loadSceneFolder;
    private Image potkuri;
    public float potkuriSpeed = 80;
    public float lastSpeed = 0;
    private float launchDelay;
    private float boost;
    private float timeDelay = 2;
    public float timeCounter =0;
    public string playerName = "";
    public GameObject giveName;
    private InputField setName;
    private bool launchGame = false;
    private GameObject loadFile1;
    private GameObject loadFile2;
    private bool audioOn = true;
    public Image audioImage;
    public Sprite audioIsOn;
    public Sprite audioIsOff;
    public AsyncOperation ao;

    // Use this for initialization
    public void Start () {
        firstFolder = GameObject.Find("FirstFolder");
        firstGame = GameObject.Find("FirstGame");
        loadGame = GameObject.Find("LoadGame");
        optionsFolder = GameObject.Find("OptionsFolder");
        giveName = GameObject.Find("GiveNameImage");
        setName = GameObject.Find("InputCompanyName").GetComponent<InputField>();
        loadFile1 = GameObject.Find("ButtonLoadFile1");
        loadFile2 = GameObject.Find("ButtonLoadFile2");
        loadSceneFolder = GameObject.Find("LoadingSceneFolder");

        firstGame.SetActive(false);
        loadGame.SetActive(false);
        optionsFolder.SetActive(false);
        giveName.SetActive(false);
        StartCoroutine(LoadNextLevelAsync(1));

        loadSceneFolder.SetActive(false);

    }

    public void NewGame()
    {
        firstFolder.SetActive(false);
        firstGame.SetActive(true);
    }
    public void LaunchGame()
    {

        playerName = setName.text;
        if (playerName != "")
        {
            PlayerPrefs.SetString("ActivePlayer", playerName);
            launchGame = true;
            timeCounter = 0;
            timeDelay = 3;
            firstGame.SetActive(false);
            loadGame.SetActive(false);
            loadSceneFolder.SetActive(true);

            


        }
        else
        {
            giveName.SetActive(true);
        }
    }
   
    public void LoadNewScene() {

        SceneManager.LoadScene(1);
    }
    IEnumerator LoadNextLevelAsync(int sceneNumber)
    {


        ao = SceneManager.LoadSceneAsync(sceneNumber, LoadSceneMode.Additive);
        ao.allowSceneActivation = false;
        yield return ao;
    }
    public void BackToMain()
    {
        firstFolder.SetActive(true);
        firstGame.SetActive(false);
        loadGame.SetActive(false);
        optionsFolder.SetActive(false);
        giveName.SetActive(false);

    }
    public void LoadGame()
    {
        
        firstFolder.SetActive(false);
        loadGame.SetActive(true);

        //koodi pelien laatamiseen
        Text[] text1;
        text1 = loadFile1.GetComponentsInChildren<Text>();
        string company1Name = "Air Line SKY";
        foreach(Text teksti in text1)
        {
            teksti.text = company1Name;
        }

        Text[] text2;
        text2 = loadFile2.GetComponentsInChildren<Text>();
        string company2Name = "Repen Firma";
        foreach (Text teksti in text2)
        {
            teksti.text = company2Name;
        }



    }

    public void ToggleAudio()
    {
        if (audioOn)
        {
            audioOn = false;
            audioImage.sprite = audioIsOff;
        }else
        {
            audioOn = true;
            audioImage.sprite = audioIsOn;
        }

    }
    public void LaunchLoadedGame(int fileNumber)
    {

    }
 
    public void Settings()
    {
        firstFolder.SetActive(false);
        optionsFolder.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();

    }


	void Update () {
       
     
      
        timeCounter += Time.deltaTime;
        
        if (timeCounter >timeDelay && launchGame)
        {
            LoadNewScene();
         
        }
	}
}
