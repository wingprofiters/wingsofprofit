﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GearRotation : MonoBehaviour {

    public Image gear1;
    public Image gear2;
    public Image gear3;
    public Text loadingText;
    public float timeFlow;
    public float rotationSpeed1;
    public float rotationSpeed2;
    public float rotationSpeed3;
    public int index;
    public List<string> loadingTexts;

    // Use this for initialization
   public void Start() { 
        loadingTexts = new List<string>();
        timeFlow = 0;
        loadingTexts.Add("Roses are red\nViolets are blue\nYour webcam is on\nAnd I'm watching you");
        loadingTexts.Add("Finding monkeys.");
        loadingTexts.Add("Feeding hamsters.");
        loadingTexts.Add("Releasing Skynet.");
        loadingTexts.Add("Sending LaserGrid \nto the matrix.");
        loadingTexts.Add("Searching Ruins.");
        loadingTexts.Add("Making profit.");
        loadingTexts.Add("Asking Fleet Commander for coffee.");
        loadingTexts.Add("Hail to the Razor!");
        loadingTexts.Add("Sharing love for beasts.");
        loadingTexts.Add("Eliminating Resistance.");
        loadingTexts.Add("Emptying Pockets in case of dungeons.");
        loadingTexts.Add("Urgh.. argh.. Thaumaturgy.");
        loadingTexts.Add("Losing valor.");
        loadingTexts.Add("Encountering coders.");
        loadingTexts.Add("Fantasizing about mushrooms.");
        loadingTexts.Add("Recruiting mexicans.");

        index = Random.Range(0, loadingTexts.Count);
        loadingText.text = loadingTexts[index];
    }
	
	// Update is called once per frame
	void Update () {
        timeFlow = timeFlow + Time.deltaTime;
        gear1.rectTransform.Rotate(0, 0, Time.deltaTime * rotationSpeed1);
        gear2.rectTransform.Rotate(0, 0, Time.deltaTime * rotationSpeed2);
        gear3.rectTransform.Rotate(0, 0, Time.deltaTime * rotationSpeed3);
        if ( timeFlow > 0.5f && timeFlow <1f)
        {
            loadingText.text = loadingTexts[index]+".";
        }
        if (timeFlow > 1f && timeFlow <1.5f){
            loadingText.text = loadingTexts[index] + "..";
        }
        if (timeFlow > 1.5f)
        {
            loadingText.text = loadingTexts[index];
            timeFlow = 0;
        }
    }
}
